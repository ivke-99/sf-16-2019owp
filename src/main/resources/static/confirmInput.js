function validateBodovi() {
    let ukupanBrojBodova = document.getElementById("ukupniBodovi").innerText
    let bodoviZaTrosenje = document.getElementById("bodoviZaTrosenje").value
    if(bodoviZaTrosenje > ukupanBrojBodova) {
        alert('Pogresno unet broj bodova');
        return false;
    }
    else {
        return true;
    }
}