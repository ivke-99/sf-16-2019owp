function validateForm() {
    let answer = confirm('Are you sure?');

    if(answer == true) {
        let uname = document.getElementById('uName');
        let password = document.getElementById('sifra');
        let email = document.getElementById('emailadresa');
        let imeKor = document.getElementById('imeUser');
        let roles = document.getElementById('role_select');
        let prezimeKorisnika = document.getElementById('prezimeKor');
        let datumRodj = document.getElementById('datum_rodj');
        let brTelefona = document.getElementById('brojFon');
        let adresaKorisnika = document.getElementById('korisnikAdresa');
        let illegalChars = /\W/; // slova, brojevi i _

        if (uname.value.toString().length < 5 || uname.value.toString().length > 15) {
            alert('Username ne moze imati vise od 15 karaktera,ili manje od 5 karaktera')
            return false;
        } else if (illegalChars.test(uname.value)) {
            uname.style.background = "Yellow";
            alert("Username moze sadrzati samo slova,brojeve i _");
            return false;
        } else if (password.value.toString().length < 6 || password.value.toString().length > 15) {
            alert('Sifra ne moze biti manja od 6 ili veca od 15 karaktera.')
            return false;
        } else if (email.value == "") {
            alert('Email ne moze biti prazan');
            return false;
        } else if (imeKor.value == "") {
            alert('Ime ne moze biti prazno');
            return false;
        } else if (roles.value == '0') {
            alert('Nista nije izabrano za ulogu korisnika.');
            return false;
        } else if (prezimeKorisnika.value == "") {
            alert('Prezime ne sme biti prazno');
            return false;
        } else if (datumRodj.value.toString() == "") {
            alert('Izaberite datum rodjenja');
            return false;
        } else if (brTelefona.value == "") {
            alert('Broj telefona ne moze biti prazan');
            return false;
        } else if (adresaKorisnika.value == "") {
            alert('Adresa ne moze biti prazna');
            return false;
        }
    }
    else {
        return false;
    }
}

function validateNarucivanjeKnjiga() {
    let kolicina = document.getElementById('kolicinaZaPorucivanje').value
    let answer = confirm('Are you sure?');
    if(answer == true) {
        if(isNaN(kolicina) || kolicina < 1) {
            alert('Proverite da li ste dobro uneli kolicinu.')
            return false;
        }
        else{
            return true;
        }
    }
}

function kupovinaKnjiga() {
	let kolicina = document.getElementById("nekaKolicinaKnjige").value
	let zaKupovanje = document.getElementById('brPrimeraka').value
    let answer = confirm('Are you sure?');
    if(answer === true) {
        if(isNaN(zaKupovanje) || zaKupovanje < 1) {
            alert('Proverite da li ste dobro uneli kolicinu.')
            return false;
        }
        
        else if(kolicina < zaKupovanje) {
        	alert('Izvinjavamo se!Trenutno nemamo vasu zeljenu kolicinu na stanju')
            return false;
        }
        else{
            return true;
        }
    }
    else{
        return false;
    }
}
function validateUnosKnjige() {

    let answer = confirm('Are you sure?');

    if(answer == true) {
        let ISBN = document.getElementById("ISBNknjige").value
        let URL = document.getElementById("URLslike").value
        let naziv = document.getElementById("nazivKnjige").value
        let izdavacka_kuca = document.getElementById("izd_kuca").value
        let autori = document.getElementById("autoriKnjige").value
        let opis = document.getElementById("opisKnjige").value
        let cena = document.getElementById("cenaKnjige").value
        let godina = document.getElementById('godinaIzdavanja').value
        let tipPoveza = document.getElementById("tipPoveza").value
        let brStr = document.getElementById("br_str").value
        let pismo = document.getElementById("pismoKnjige").value
        let jezik = document.getElementById("jezik").value
        let ocena = document.getElementById('prosecna_ocena').value
        let brojPrimeraka = document.getElementById('brojPrimeraka').value
        if (URL == "") {
            alert('URL slike ne moze biti prazan.')
            return false;
        } else if (ISBN == "") {
            alert('ISBN ne moze biti prazan.')
            return false;
        } else if (naziv == "") {
            alert('Naziv ne moze biti prazan.')
            return false;
        } else if (izdavacka_kuca == "") {
            alert('Izdavacka kuca ne moze biti prazna.')
            return false;
        } else if (autori == "") {
            alert('Navedite autore knjige.')
            return false;
        } else if (opis == "") {
            alert('Opis je prazan.')
            return false;
        } else if (cena == "") {
            alert('Cena ne moze biti prazna.')
            return false;
        } else if (tipPoveza == "") {
            alert('Tip poveza nije dobro izabran.')
            return false;
        } else if (pismo == "") {
            alert('Pismo nije dobro izabrano.')
            return false;
        } else if (brStr == "") {
            alert('Navedite broj stranica.')
            return false;
        } else if (jezik == "") {
            alert('Unesite jezik knjge.')
            return false;
        }
        else if(godina == "") {
            alert('Unesite godinu.')
            return false;
        }
    }

    else {
        return false;
    }


}

