function sortTable(n, tableClass) {
    var table, rows, switching, i, x, y, shouldSwitch, dir, switchcount = 0;

    table = document.getElementById(tableClass);
    switching = true;
    //default sorting direkcija je asc
    dir = "asc";
    //loop se desava dok god se ne trigeruje switch
    while (switching) {
        switching = false;
        rows = table.getElementsByTagName("tr");
        //izbecicemo prvi element, jer nema smisla da se sortira po slici
        for (i = 1; i < (rows.length - 1); i++) {
            //za pocetak postavimo shouldswitch na false jer ne znamo da li ce switchovati 2 elementa
            shouldSwitch = false;
            //u redu uzimamo td elemente
            x = rows[i].getElementsByTagName("td")[n];
            y = rows[i + 1].getElementsByTagName("td")[n];
            //pitamo da li je broj, ako je broj parsiracemo ga i uporedjivacemo ga float, ako nije
            //uporedjivacemo ga po abecedi
            var cmpX=isNaN(parseFloat(x.innerHTML))?x.innerHTML.toLowerCase():parseFloat(x.innerHTML);
            var cmpY=isNaN(parseFloat(y.innerHTML))?y.innerHTML.toLowerCase():parseFloat(y.innerHTML);
            //radi reda provera da li poseduje - karakter jer on poremeti sorting, ako poseduje postavimo vrednost na
            //0 ako ne samo nastavimo
            cmpX=(cmpX=='-')?0:cmpX;
            cmpY=(cmpY=='-')?0:cmpY;
            if (dir == "asc") {
                if (cmpX > cmpY) {
                    shouldSwitch= true;
                    break;
                }
            } else if (dir == "desc") {
                if (cmpX < cmpY) {
                    shouldSwitch= true;
                    break;
                }
            }
        }
        if (shouldSwitch) {
            //ako smo potrefili neki od gore uslova, zameni kolone
            rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
            switching = true;
            //povecaj count odradjenih sviceva za 1
            switchcount ++;
        } else {
            //ukoliko nismo potrefili ni jedan uslov, i switchcount je 0 daj mu desc i zavrti loop opet
            if (switchcount == 0 && dir == "asc") {
                dir = "desc";
                switching = true;
            }
        }
    }
}