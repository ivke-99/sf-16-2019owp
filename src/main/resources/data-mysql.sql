INSERT INTO user (`id`,`active`,`adresa`,`br_tel`,`datum_rodjenja`,`datumvreme_registracije`,`email`,`ime`,`password`,`prezime`,`roles`,`user_name`)
VALUES (1,1,'Marka Zmijanca 3','063528394','1993-03-03','2020-03-03 15:30:00.000000','marko1@gmail.com','Marko','marko1234','Pijac','ROLE_ADMIN','mark1234');
INSERT INTO user (`id`,`active`,`adresa`,`br_tel`,`datum_rodjenja`,`datumvreme_registracije`,`email`,`ime`,`password`,`prezime`,`roles`,`user_name`)
VALUES (2,1,'Njegoseva 5','061321548','1993-05-03','2020-05-03 15:30:00.000000','miljana@gmail.com','Miljana','123456','Gnjocevic','ROLE_USER','miljka123');
INSERT into user (`id`,`active`,`adresa`,`br_tel`,`datum_rodjenja`,`datumvreme_registracije`,`email`,`ime`,`password`,`prezime`,`roles`,`user_name`)
VALUES (3,1,'Boraca 2/B','069842135','1992-05-03','2020-06-03 15:30:00.000000','korisnik@gmail.com','Dejan','123456','Vrabac','ROLE_USER','vrabac123');


INSERT INTO zanr(`id`, `naziv`, `opis`)
VALUES(1,'Komedija','Sa ovim zanrom cete se nasmejati, i biti uvek radosni!');
INSERT INTO zanr(`id`, `naziv`, `opis`)
VALUES(2,'Klasika','Za sve ljubitelje klasicnih knjiga!');
INSERT INTO zanr(`id`, `naziv`, `opis`)
VALUES(3,'Drama','Ovaj zanr vas nece ostaviti ravnodusnim!');
INSERT INTO zanr(`id`, `naziv`, `opis`)
VALUES(4,'Avantura','Osetite mirise dalekih planina, i neistrazenih mora!');

INSERT INTO knjiga (`isbn`,`autori`,`broj_stranica`,`cena`,`godina_izdavanja`,`izdavacka_kuca`,`jezik_knjige`,`kolicina`,`naziv`,`opis`,`pismo`,`prosecna_ocena`,`slika`,`tip_poveza`)
VALUES ('9788610034226','Ivo Andrić',352,333,1992,'Laguna','srpski',20,'Na Drini ćuprija','Sudbina Mehmed-paše Sokolovića predodredila je da njegov život bude prekinut pre otelotvorenja njegove ideje na javi.','cirilica',2,'https://i.imgur.com/kTquRc0.jpg','meki');

INSERT INTO knjiga (`isbn`,`autori`,`broj_stranica`,`cena`,`godina_izdavanja`,`izdavacka_kuca`,`jezik_knjige`,`kolicina`,`naziv`,`opis`,`pismo`,`prosecna_ocena`,`slika`,`tip_poveza`)
VALUES ('9781847496447','Herman Melville',342,500,2015,'Laguna','engleski',35,'Moby Dick','When the young Ishmael gets on board Captain Ahab''s whaling ship, little does he suspect that the mission..','cirilica',4,'https://i.imgur.com/LfrIt2t.jpeg','meki');

INSERT INTO knjiga (`isbn`,`autori`,`broj_stranica`,`cena`,`godina_izdavanja`,`izdavacka_kuca`,`jezik_knjige`,`kolicina`,`naziv`,`opis`,`pismo`,`prosecna_ocena`,`slika`,`tip_poveza`)
VALUES ('9788673105260','Benjamin Jakobus',350,1000,2015,'Kompjuter biblioteka','srpski',0,'Bootstrap 4','Ova knjiga će vam pomoći da upotrebite i prilagodite Bootstrap za kreiranje privlačnih veb sajtova koji odgovaraju vašim potrebama.','cirilica',4,'https://i.imgur.com/gNvXh3p.jpg','tvrdi');

INSERT INTO knjiga (`isbn`,`autori`,`broj_stranica`,`cena`,`godina_izdavanja`,`izdavacka_kuca`,`jezik_knjige`,`kolicina`,`naziv`,`opis`,`pismo`,`prosecna_ocena`,`slika`,`tip_poveza`)
VALUES ('9788842931539','Stefania Auci',233,1170,2015,'Editrice Nord','srpski',55,'I leoni di Sicilia','Roman o porodici Florio, nekrunisanim kraljevima Sicilije. Bila jednom jedna porodica koja se suprotstavila postojećem..','cirilica',4,'https://i.imgur.com/zT6iM5L.jpg','tvrdi');

INSERT INTO knjiga (`isbn`,`autori`,`broj_stranica`,`cena`,`godina_izdavanja`,`izdavacka_kuca`,`jezik_knjige`,`kolicina`,`naziv`,`opis`,`pismo`,`prosecna_ocena`,`slika`,`tip_poveza`)
VALUES ('9788673105505','Federico Kereki',233,1130,2015,'Kompjuter biblioteka','srpski',333,'JavaScript funkcionalno programiranje','Funkcionalno programiranje je paradigma za razvoj softvera sa boljim performansama. Ono pomaže da napišete sažet kod i kod koji se može testirati.','cirilica',3,'https://i.imgur.com/ZMXbcRl.jpg','tvrdi');

INSERT INTO knjiga_zanr
values ('9788610034226',1);
INSERT INTO knjiga_zanr
values ('9788610034226',2);
INSERT INTO knjiga_zanr
values ('9781847496447',2);

INSERT INTO kupljena_knjiga (`id`,`broj_primeraka`,`cena`,`knjiga_isbn`) VALUES (1,7,3500,'9781847496447');
INSERT INTO kupljena_knjiga (`id`,`broj_primeraka`,`cena`,`knjiga_isbn`) VALUES (2,8,2664,'9788610034226');
INSERT INTO kupljena_knjiga (`id`,`broj_primeraka`,`cena`,`knjiga_isbn`) VALUES (3,9,10530,'9788842931539');
INSERT INTO kupljena_knjiga (`id`,`broj_primeraka`,`cena`,`knjiga_isbn`) VALUES (4,8,9040,'9788673105505');

INSERT INTO kupovina (`id`,`ukupna_cena`,`datum_kupovine`,`user_id`) VALUES (1,6164,'2021-02-01',2);
INSERT INTO kupovina (`id`,`ukupna_cena`,`datum_kupovine`,`user_id`) VALUES (2,19570,'2020-02-01',2);

INSERT INTO kupovina_knjige (`kupljenaID`,`kupovinaID`) VALUES (1,1);
INSERT INTO kupovina_knjige (`kupljenaID`,`kupovinaID`) VALUES (2,1);
INSERT INTO kupovina_knjige (`kupljenaID`,`kupovinaID`) VALUES (3,2);
INSERT INTO kupovina_knjige (`kupljenaID`,`kupovinaID`) VALUES (4,2);

