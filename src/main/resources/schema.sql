DROP SCHEMA IF EXISTS knjizaradb;
CREATE SCHEMA knjizaradb DEFAULT CHARACTER SET utf8;
USE knjizaradb;

CREATE TABLE user (
                      id int AUTO_INCREMENT,
                      active tinyint(1) NOT NULL,
                      adresa varchar(255) NOT NULL,
                      br_tel varchar(255) NOT NULL,
                      datum_rodjenja date NOT NULL,
                      datumvreme_registracije datetime(6) DEFAULT NULL,
                      email varchar(255) NOT NULL,
                      ime varchar(255) NOT NULL,
                      password varchar(255) NOT NULL,
                      prezime varchar(255) NOT NULL,
                      roles varchar(255) NOT NULL,
                      user_name varchar(255) NOT NULL,
                      primary key (id,user_name)
);

CREATE TABLE knjiga (
                        isbn varchar(13) PRIMARY KEY,
                        autori varchar(255) NOT NULL,
                        broj_stranica int NOT NULL,
                        cena double NOT NULL,
                        godina_izdavanja int NOT NULL,
                        izdavacka_kuca varchar(255) NOT NULL,
                        jezik_knjige varchar(255) NOT NULL,
                        kolicina int DEFAULT NULL,
                        naziv varchar(255) NOT NULL,
                        opis varchar(255) NOT NULL,
                        pismo varchar(255) NOT NULL,
                        prosecna_ocena double DEFAULT 0,
                        slika varchar(255) NOT NULL,
                        tip_poveza varchar(255) NOT NULL
);

CREATE TABLE zanr (
                      id int AUTO_INCREMENT PRIMARY KEY,
                      naziv varchar(255) NOT NULL,
                      opis varchar(255) NOT NULL
);

CREATE TABLE knjiga_zanr (
                             knjigaID varchar(13),
                             zanrID int,
                             PRIMARY KEY(knjigaID, zanrID),
                             FOREIGN KEY(knjigaID) REFERENCES knjiga(isbn),
                             FOREIGN KEY(zanrId) REFERENCES zanr(id)
);

CREATE TABLE kupljena_knjiga (
                                 id int AUTO_INCREMENT PRIMARY KEY,
                                 broj_primeraka int NOT NULL,
                                 cena double NOT NULL,
                                 knjiga_isbn varchar(13) NOT NULL,
                                 FOREIGN KEY (knjiga_isbn) REFERENCES knjiga (isbn)
);

CREATE TABLE kupovina (
                          id int AUTO_INCREMENT PRIMARY KEY,
                          ukupna_cena double not null,
                          datum_kupovine date not null,
                          user_id int not null,
                          FOREIGN KEY(user_id) REFERENCES user (id)
);

create table kupovina_knjige (
                                 kupljenaID int,
                                 kupovinaID int,
                                 PRIMARY KEY(kupljenaID, kupovinaID),
                                 FOREIGN KEY (kupljenaID) REFERENCES kupljena_knjiga(id),
                                 FOREIGN KEY (kupovinaID) REFERENCES kupovina(id)
);

create table komentar (
    id_kom int primary key AUTO_INCREMENT,
    id_user int not null,
    opis varchar(255) not null,
    ISBN_knjige char(13) not null,
    ocena_korisnika int not null,
    status varchar(20) not null,
    FOREIGN KEY (id_user) references user(id),
    FOREIGN KEY (ISBN_knjige) references knjiga(isbn)
);

create table loyalty_card (
    id int primary key AUTO_INCREMENT,
    id_user int not null,
    br_poena int not null,
    status varchar(255) not null,
    FOREIGN KEY (id_user) references knjizaradb.user(id)
);

create table wish(
    id_wish   int primary key AUTO_INCREMENT,
    id_user   int      not null,
    book_isbn char(13) not null,
    FOREIGN KEY (id_user) references user (id),
    FOREIGN KEY (book_isbn) references knjiga (isbn)
);

create table special_popust (
    datum  date primary key,
    popust float not null
);