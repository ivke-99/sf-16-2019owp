package com.ftn.web.model;

import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;
import java.util.List;

@Data
public class Kupovina {
    private int idKupovine;
    private Double ukupnaCena;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate datumKupovine;
    private User korisnik;
    private List<KupljenaKnjiga> kupljenaKnjigaList;
}
