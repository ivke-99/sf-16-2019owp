package com.ftn.web.model;

import com.ftn.web.repository.ZanrRepository;
import com.ftn.web.util.NotExistingISBN;
import lombok.Data;

import javax.validation.constraints.*;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

@Data
public class KnjigaDTO {
    @NotBlank(message = "Naziv ne moze biti prazan.")
    private String naziv;
    @NotBlank(message = "Morate uneti ISBN.")
    @Size(min = 13, max = 13, message = "ISBN mora imati 13 karaktera.")
    @NotExistingISBN
    private String ISBN;
    @NotBlank(message = "Izdavacka kuca ne moze biti prazna.")
    private String izdavacka_kuca;
    @NotBlank(message = "Mora postojati autor.")
    private String autori;
    @Positive(message = "Mora biti pozitivan broj.")
    @NotNull(message = "Morate uneti godinu.")
    private Integer godina_izdavanja;
    @NotBlank(message = "Opis ne moze biti prazan.")
    private String opis;
    private String slika;
    @Positive(message = "Cena mora biti pozitivan broj.")
    @NotNull(message = "Morate uneti cenu.")
    private Double cena;
    @Positive(message = "Broj stranica mora biti pozitivan broj.")
    @NotNull(message = "Morate uneti broj stranica.")
    private Integer broj_stranica;
    @NotBlank(message = "Odaberite tip poveza.")
    private String tip_poveza;
    @NotBlank(message = "Odaberite pismo.")
    private String pismo;
    @NotBlank(message = "Jezik knjige ne moze biti prazan.")
    private String jezikKnjige;
    private Double prosecnaOcena;
    private int kolicina;
    @NotNull(message = "Knjiga mora imati bar 1 zanr.")
    private List<Integer> zanrList;
}
