package com.ftn.web.model;

import lombok.Data;

@Data
public class LoyaltyCard {
    private int ID;
    private User user;
    private int brojBodova;
    private String status;
}
