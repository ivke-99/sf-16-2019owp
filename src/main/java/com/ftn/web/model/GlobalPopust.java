package com.ftn.web.model;

import com.ftn.web.util.NotExistingDate;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.Past;
import java.time.LocalDate;

@Data
public class GlobalPopust {
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @NotExistingDate
    @Past(message = "Mora biti u buducnosti.")
    private LocalDate danPopusta;
    private Float popust;
}
