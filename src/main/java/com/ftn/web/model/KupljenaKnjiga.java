package com.ftn.web.model;

import lombok.Data;


@Data
public class KupljenaKnjiga {
    public int ID;
    private Knjiga knjiga;
    private int brojPrimeraka;
    private Double cena;
}
