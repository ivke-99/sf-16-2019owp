package com.ftn.web.model;

import lombok.Data;

@Data
public class Izvestaj {
    private String isbn;
    private String naziv;
    private String autori;
    private Integer ukupanBroj;
    private Double ukupnaCena;
}
