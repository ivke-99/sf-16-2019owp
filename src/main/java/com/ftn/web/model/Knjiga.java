package com.ftn.web.model;

import lombok.Data;


import java.util.List;


@Data
public class Knjiga {
    private String naziv;
    private String ISBN;
    private String izdavacka_kuca;
    private String autori;
    private Integer godina_izdavanja;
    private String opis;
    private String slika;
    private Double cena;
    private Integer broj_stranica;
    private String tip_poveza;
    private String pismo;
    private String jezikKnjige;
    private Double prosecnaOcena;
    private int kolicina;
    private List<Zanr> zanrList;
}
