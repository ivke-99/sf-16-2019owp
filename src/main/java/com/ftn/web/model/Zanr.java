package com.ftn.web.model;

import lombok.Data;

@Data
public class Zanr {
    private int id;
    private String naziv;
    private String opis;
}
