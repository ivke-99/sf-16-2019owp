package com.ftn.web.model;

import lombok.Data;


@Data
public class Komentar {
    private int ID;
    private User korisnik;
    private String komentar;
    private int ocena;
    private String status;
    private Knjiga knjiga;
}
