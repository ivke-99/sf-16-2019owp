package com.ftn.web.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Wish {
    private int id;
    private Knjiga knjiga;
    private User user;
}
