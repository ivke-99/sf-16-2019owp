package com.ftn.web.model;

import com.ftn.web.util.NotExistingUsername;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Past;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Data
public class UserDTO {
    @NotBlank(message="Username cannot be empty.")
    @NotExistingUsername
    private String userName;
    @NotBlank(message="Password cannot be empty.")
    private String password;
    @NotBlank(message="Name cannot be empty.")
    private String ime;
    @NotBlank(message="Surname cannot be empty.")
    private String prezime;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @Past(message = "It is still not possible to be born in the future.")
    private LocalDate datum_rodjenja;
    @Email
    private String email;
    @NotBlank(message="Adress cannot be empty.")
    private String adresa;
    @NotBlank(message="Telephone number cannot be empty.")
    private String br_tel;
}
