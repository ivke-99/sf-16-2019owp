package com.ftn.web.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.util.*;
import java.util.concurrent.CopyOnWriteArrayList;

@Component
@Scope(value="session", proxyMode = ScopedProxyMode.TARGET_CLASS)
public class KorisnickaKorpa implements Serializable {
    private final HashSet<KupljenaKnjiga> kupljenaKnjigaList = new HashSet<>();


    public void dodajUKorpu(KupljenaKnjiga kupljenaKnjiga) {
        if(kupljenaKnjigaList.stream().noneMatch(e -> e.getKnjiga().getISBN().equals(kupljenaKnjiga.getKnjiga().getISBN()))) {
            kupljenaKnjigaList.add(kupljenaKnjiga);
        }
        else {
            KupljenaKnjiga knjiga = kupljenaKnjigaList.stream().filter(f -> f.getKnjiga().getISBN().equals(kupljenaKnjiga.getKnjiga().getISBN())).findFirst().get();
            knjiga.setBrojPrimeraka(knjiga.getBrojPrimeraka()+ kupljenaKnjiga.getBrojPrimeraka());
            knjiga.setCena(knjiga.getCena() + kupljenaKnjiga.getCena());
        }
    }

    public void dropCart() {
        kupljenaKnjigaList.clear();
    }

    public void removeFromCart(KupljenaKnjiga kupljenaKnjigak) {
        kupljenaKnjigaList.remove(kupljenaKnjigak);
    }

    public Double getUkupnaCena() {
        return kupljenaKnjigaList.stream().mapToDouble(KupljenaKnjiga::getCena).sum();
    }

    public KupljenaKnjiga getById(int id) {
        return kupljenaKnjigaList.stream().filter(e-> e.getID() == id).findFirst().orElseThrow(NoSuchElementException::new);
    }

    public HashSet<KupljenaKnjiga> getKupljenaKnjigaList() {
        return new HashSet<>(this.kupljenaKnjigaList);
    }
}
