package com.ftn.web.service;

import com.ftn.web.model.Zanr;

import java.util.List;

public interface ZanrService {
    Zanr findByID(int ID);
    List<Zanr> findAll();
}
