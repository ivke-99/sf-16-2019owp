package com.ftn.web.service.impl;

import com.ftn.web.model.MyUserDetails;
import com.ftn.web.model.User;
import com.ftn.web.repository.UserRepository;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class MyUserDetailsService implements UserDetailsService {

    private final UserRepository userRepository;

    public MyUserDetailsService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException {
       Optional<User> user = userRepository.findByUserName(userName);
       user.orElseThrow(() -> new UsernameNotFoundException("Not found:" + userName));
       return user.map(MyUserDetails::new).get();
    }
}
