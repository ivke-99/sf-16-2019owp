package com.ftn.web.service.impl;

import com.ftn.web.model.Knjiga;
import com.ftn.web.model.Komentar;
import com.ftn.web.repository.KomentarRepository;
import com.ftn.web.service.KomentarService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;

@Service
public class KomentarServiceImpl implements KomentarService {
    private final KomentarRepository komentarRepository;

    public KomentarServiceImpl(KomentarRepository komentarRepository) {
        this.komentarRepository = komentarRepository;
    }

    @Override
    public void save(Komentar komentar) {
        komentar.setStatus("cekanje");
        komentarRepository.save(komentar);
    }

    @Override
    public List<Komentar> findByBook(Knjiga knjiga) {
        return komentarRepository.findAll().stream().filter(f-> f.getKnjiga().getISBN().equals(knjiga.getISBN())).collect(Collectors.toList());
    }

    @Override
    public void obradiKomentar(int id,String status) {
        Komentar kom = komentarRepository.findById(id).orElseThrow(NoSuchElementException::new);
        kom.setStatus(status);
        komentarRepository.process(kom);
    }
}
