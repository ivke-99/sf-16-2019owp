package com.ftn.web.service.impl;

import com.ftn.web.model.Knjiga;
import com.ftn.web.model.KorisnickaKorpa;
import com.ftn.web.model.KupljenaKnjiga;
import com.ftn.web.repository.KupljenaKnjigaRepository;
import com.ftn.web.service.KupljenaKnjigaService;
import lombok.val;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import java.util.NoSuchElementException;
import java.util.stream.Collectors;

@Service
public class KupljenaKnjigaServiceImpl implements KupljenaKnjigaService {

    @Autowired
    private KorisnickaKorpa korisnickaKorpa;
    private final KupljenaKnjigaRepository kupljenaKnjigaRepository;

    public KupljenaKnjigaServiceImpl(KupljenaKnjigaRepository kupljenaKnjigaRepository) {
        this.kupljenaKnjigaRepository = kupljenaKnjigaRepository;
    }

    @Override
    public KupljenaKnjiga kreirajZaKupovinu(Knjiga knjiga, int brojPrimeraka) {
        //komentari nista ne valjaju, ne valja prihvatanje komentara
        KupljenaKnjiga k = new KupljenaKnjiga();
        // ne znam sta ovo radi
        val sveKupljene = kupljenaKnjigaRepository.findAll();
        if(sveKupljene.isEmpty()) {
            if(korisnickaKorpa.getKupljenaKnjigaList().size() == 0) {
                k.setID(1);
            }
            else {
                val lastID = korisnickaKorpa.getKupljenaKnjigaList().stream().skip(korisnickaKorpa.getKupljenaKnjigaList().size() - 1).findFirst().get();
                k.setID(lastID.getID() + 1);
            }
        }
        else{
                if(korisnickaKorpa.getKupljenaKnjigaList().size() == 0) {
                    val nestozadnje = sveKupljene.stream().mapToInt(KupljenaKnjiga::getID).max().orElseThrow(NoSuchElementException::new);
                    k.setID(nestozadnje + 1);
                }
                else {
                    val nestojosnazadnije = korisnickaKorpa.getKupljenaKnjigaList().stream().mapToInt(KupljenaKnjiga::getID).max().orElseThrow(NoSuchElementException::new);
                    k.setID(nestojosnazadnije + 1);
                }
        }
        k.setKnjiga(knjiga);
        k.setBrojPrimeraka(brojPrimeraka);
        k.setCena(knjiga.getCena() * brojPrimeraka);
        return k;
    }


	@Override
	public KupljenaKnjiga findByID(int id) {
		return kupljenaKnjigaRepository.findById(id).orElseThrow(NoSuchElementException::new);
	}

}
