package com.ftn.web.service.impl;

import com.ftn.web.model.KorisnickaKorpa;
import com.ftn.web.model.KupljenaKnjiga;
import com.ftn.web.model.User;
import com.ftn.web.repository.UserRepository;
import com.ftn.web.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.NoSuchElementException;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    KorisnickaKorpa korisnickaKorpa;

    private final UserRepository userRepository;

    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public List<User> findAll() {
        return userRepository.findAll();
    }

    @Override
    public User findByUserName(String userName) {
        return userRepository.findByUserName(userName).orElseThrow(NoSuchElementException::new);
    }

    @Override
    public void saveUser(User user) {
        user.setDatumvreme_registracije(LocalDateTime.now());
        user.setActive(true);
        userRepository.save(user);
    }

    public void updateUser(User user) {
        userRepository.update(user);
    }

    @Override
    public User getUserbyId(int id) {
        return userRepository.getUserById(id);
    }

    @Override
    public boolean isUsernameAlreadyInUse(String username) {
        return userRepository.findAll().stream().noneMatch(f -> f.getUserName().equals(username));
    }

    @Override
    public void addToCart(KupljenaKnjiga knjiga,User user) {
        korisnickaKorpa.dodajUKorpu(knjiga);
    }
}
