package com.ftn.web.service.impl;

import com.ftn.web.model.Knjiga;
import com.ftn.web.model.Zanr;
import com.ftn.web.repository.KnjigaRepository;
import com.ftn.web.service.KnjigaService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

@Service
public class KnjigaServiceImpl implements KnjigaService {

    private final KnjigaRepository knjigaRepository;

    public KnjigaServiceImpl(KnjigaRepository knjigaRepository) {
        this.knjigaRepository = knjigaRepository;
    }

    @Override
    public Knjiga findByISBN(String ISBN) {
        return knjigaRepository.findByISBN(ISBN).orElseThrow(NoSuchElementException::new);
    }

    @Override
    public void poruciKnjigu(Integer kolicina,Knjiga knjiga) {
        knjiga.setKolicina(knjiga.getKolicina() + kolicina);
        knjigaRepository.poruciKnjigu(knjiga);
    }

    @Override
    public void save(Knjiga knjiga) {
        knjigaRepository.create(knjiga);
    }

    @Override
    public void update(Knjiga knjiga) {
        knjigaRepository.update(knjiga);
    }

    @Override
    public List<Knjiga> findAll() {
        return knjigaRepository.findAll();
    }

    @Override
    public List<String> findAllISBN() {
        return knjigaRepository.findAllISBN();
    }

    @Override
    public boolean isISBNAlreadyInUse(String ISBN) {
            return knjigaRepository.findAll().stream().noneMatch(f -> f.getISBN().equals(ISBN));
    }

}
