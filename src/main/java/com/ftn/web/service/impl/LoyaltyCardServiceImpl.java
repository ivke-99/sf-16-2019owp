package com.ftn.web.service.impl;

import com.ftn.web.model.LoyaltyCard;
import com.ftn.web.model.User;
import com.ftn.web.repository.LoyaltyRepository;
import com.ftn.web.service.LoyaltyCardService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.NoSuchElementException;

@Service
public class LoyaltyCardServiceImpl implements LoyaltyCardService {

    @Autowired
    private LoyaltyRepository loyaltyRepository;

    @Override
    public void obradi(LoyaltyCard loyaltyCard) {
        if(loyaltyCard.getStatus().equals("prihvacena")) {
            loyaltyCard.setBrojBodova(4);
        }
        loyaltyRepository.process(loyaltyCard);
    }

    @Override
    public void uvecajBodove(LoyaltyCard loyaltyCard) {
        int bodovi;
    }

    @Override
    public LoyaltyCard findByUser(User user) {
        return loyaltyRepository.findByUser(user).orElseThrow(NoSuchElementException::new);
    }
}
