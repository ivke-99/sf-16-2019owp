package com.ftn.web.service.impl;

import com.ftn.web.model.Zanr;
import com.ftn.web.repository.ZanrRepository;
import com.ftn.web.service.ZanrService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.NoSuchElementException;
@Service
public class ZanrServiceImpl implements ZanrService {

    private final ZanrRepository zanrRepository;

    public ZanrServiceImpl(ZanrRepository zanrRepository) {
        this.zanrRepository = zanrRepository;
    }

    @Override
    public Zanr findByID(int ID) {
        return zanrRepository.findById(ID).orElseThrow(NoSuchElementException::new);
    }

    @Override
    public List<Zanr> findAll() {
        return zanrRepository.findAll();
    }
}
