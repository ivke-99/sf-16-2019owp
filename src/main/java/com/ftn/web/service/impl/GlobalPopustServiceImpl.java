package com.ftn.web.service.impl;

import com.ftn.web.model.GlobalPopust;
import com.ftn.web.repository.GlobalPopustRepository;
import com.ftn.web.service.GlobalPopustService;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.Optional;

@Service
public class GlobalPopustServiceImpl implements GlobalPopustService {

    private final GlobalPopustRepository globalPopustRepository;

    public GlobalPopustServiceImpl(GlobalPopustRepository globalPopustRepository) {
        this.globalPopustRepository = globalPopustRepository;
    }

    @Override
    public boolean checkIfTodayIsSpecial() {
        GlobalPopust optional = globalPopustRepository.findOne(LocalDate.now().toString());
        return optional != null;
    }

    @Override
    public boolean checkIfGlobalPopustExists(String selectedDate) {
        GlobalPopust optional = globalPopustRepository.findOne(selectedDate);
        if(optional == null) {
            return false;
        }
        else{
            return true;
        }
    }
}
