package com.ftn.web.service.impl;

import com.ftn.web.model.*;
import com.ftn.web.repository.GlobalPopustRepository;
import com.ftn.web.repository.KomentarRepository;
import com.ftn.web.repository.KupovinaRepository;
import com.ftn.web.repository.LoyaltyRepository;
import com.ftn.web.service.KnjigaService;
import com.ftn.web.service.KomentarService;
import com.ftn.web.service.KupovinaService;
import com.ftn.web.service.UserService;
import lombok.val;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

@Service
public class KupovinaServiceImpl implements KupovinaService {


    @Autowired
    KorisnickaKorpa korisnickaKorpa;
    private final KomentarService komentarService;
    private final KnjigaService knjigaService;
    private final KupovinaRepository kupovinaRepository;
    private final UserService userService;
    private final KomentarRepository komentarRepository;
    private final LoyaltyRepository loyaltyRepository;
    private final GlobalPopustRepository globalPopustRepository;

    public KupovinaServiceImpl(KomentarService komentarService, KnjigaService knjigaService, KupovinaRepository kupovinaRepository, UserService userService, KomentarRepository komentarRepository, LoyaltyRepository loyaltyRepository, GlobalPopustRepository globalPopustRepository) {
        this.komentarService = komentarService;
        this.knjigaService = knjigaService;
        this.kupovinaRepository = kupovinaRepository;
        this.userService = userService;
        this.komentarRepository = komentarRepository;
        this.loyaltyRepository = loyaltyRepository;
        this.globalPopustRepository = globalPopustRepository;
    }

    @Override
    public Kupovina findById(Integer id) {
        return kupovinaRepository.findById(id).orElseThrow(NoSuchElementException::new);
    }



    private Integer uvecajBodove(Double ukupnaCena) {
        return (int) (ukupnaCena/1000);
    }

    private Double iskoristiBodove(Double ukupnaCena, Integer brojBodova) {
        do{
          ukupnaCena = ukupnaCena - ukupnaCena * (0.05);
          brojBodova = brojBodova - 1;
        }while(brojBodova != 0);
            return ukupnaCena;
    }

    private Kupovina namestiDatumiID() {
        Kupovina k = new Kupovina();
        //rucno namestanje id-a zbog sesije
        val listasvekupovina = kupovinaRepository.findAll();
        if(listasvekupovina.isEmpty()) {
            k.setIdKupovine(1);
        }
        else{
            val lastInt = listasvekupovina.stream().skip(listasvekupovina.size() - 1).findFirst().get();
            k.setIdKupovine(lastInt.getIdKupovine() + 1);
        }
        k.setDatumKupovine(LocalDate.now());
        return k;
    }

    private void umanjiKolicineKnjiga() {
        //iterira kroz korpu i za svaku kupljenu knjigu pristupa knjizi i smanjuje
        //kolicinu i menja podatak u bazi
        for(KupljenaKnjiga kj : korisnickaKorpa.getKupljenaKnjigaList()) {
            Knjiga knjiga = kj.getKnjiga();
            knjiga.setKolicina(knjiga.getKolicina() - kj.getBrojPrimeraka());
            // ne porucujemo vec brisemo kupljene
            knjigaService.poruciKnjigu(knjiga.getKolicina() - kj.getBrojPrimeraka(), knjiga);
        }
    }

    @Override
    public void zavrsiKupovinu(User user,int brojBodovaZaTrosenje) {
        Kupovina k = namestiDatumiID();
        //vidimo koji je broj bodova ako je 0 uvecavamo
        if(brojBodovaZaTrosenje == 0) {
            int bodovi = uvecajBodove(korisnickaKorpa.getUkupnaCena());
            loyaltyRepository.increasePoints(user,bodovi);
            k.setUkupnaCena(korisnickaKorpa.getUkupnaCena());
        }
        else{
            //umanji cenu kupovine
            Double cena = iskoristiBodove(korisnickaKorpa.getUkupnaCena(), brojBodovaZaTrosenje);
            k.setUkupnaCena(cena);
            // decrease by broj za trosenje
            loyaltyRepository.decreasePoints(user, brojBodovaZaTrosenje);
        }
        k.setKorisnik(user);
        ArrayList<KupljenaKnjiga> lista = new ArrayList<>(korisnickaKorpa.getKupljenaKnjigaList());
        k.setKupljenaKnjigaList(lista);
        umanjiKolicineKnjiga();
        kupovinaRepository.save(k);
        korisnickaKorpa.dropCart();
    }

    @Override
    public void zavrsiSaGlobalPopust(User user) {
        GlobalPopust globalPopust = globalPopustRepository.findOne(LocalDate.now().toString());
        Kupovina k = namestiDatumiID();
        Double ukupnaCena = korisnickaKorpa.getUkupnaCena() - korisnickaKorpa.getUkupnaCena() * globalPopust.getPopust();
        k.setUkupnaCena(ukupnaCena);
        k.setKorisnik(user);
        umanjiKolicineKnjiga();
        ArrayList<KupljenaKnjiga> lista = new ArrayList<>(korisnickaKorpa.getKupljenaKnjigaList());
        k.setKupljenaKnjigaList(lista);
        kupovinaRepository.save(k);
        korisnickaKorpa.dropCart();
    }

    @Override
    public List<Kupovina> korisnikoveKupovine(User user) {
        return kupovinaRepository.getUserPurchases(user);
    }

    @Override
    public boolean checkIfBought(Knjiga knjiga, User user) {
        List<Kupovina> korKupovine = korisnikoveKupovine(user);
        List<Komentar> sviKomentari = komentarRepository.findAll();
        boolean isBought = false;
        for(Kupovina k1 : korKupovine) {
            for(KupljenaKnjiga k2 : k1.getKupljenaKnjigaList()) {
                if (k2.getKnjiga().getISBN().equals(knjiga.getISBN())) {
                    isBought = true;
                    boolean isCommented = sviKomentari.stream().anyMatch(f-> f.getKnjiga().getISBN().equals(knjiga.getISBN()) && f.getKorisnik().getUserName().equals(user.getUserName()));
                    if(isCommented) {
                        isBought = false;
                    }
                    break;
                }
            }
        }
        return isBought;
    }


}
