package com.ftn.web.service;

import com.ftn.web.model.Knjiga;
import com.ftn.web.model.Zanr;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public interface KnjigaService {
    Knjiga findByISBN(String ISBN);
    void poruciKnjigu(Integer kolicina,Knjiga knjiga);
    void save(Knjiga knjiga);
    void update(Knjiga knjiga);
    List<Knjiga> findAll();
    List<String> findAllISBN();
    boolean isISBNAlreadyInUse(String ISBN);
}
