package com.ftn.web.service;

import com.ftn.web.model.Knjiga;
import com.ftn.web.model.Komentar;

import java.util.List;

public interface KomentarService {
    void save(Komentar komentar);
    List<Komentar> findByBook(Knjiga knjiga);
    void obradiKomentar(int ID,String status);
}
