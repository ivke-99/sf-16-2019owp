package com.ftn.web.service;

import com.ftn.web.model.Knjiga;
import com.ftn.web.model.KupljenaKnjiga;

public interface KupljenaKnjigaService {
    KupljenaKnjiga kreirajZaKupovinu(Knjiga knjiga,int brojPrimeraka);
    KupljenaKnjiga findByID(int id);
}
