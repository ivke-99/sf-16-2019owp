package com.ftn.web.service;

import com.ftn.web.model.GlobalPopust;

public interface GlobalPopustService {
    boolean checkIfTodayIsSpecial();
    boolean checkIfGlobalPopustExists(String selectedDate);
}
