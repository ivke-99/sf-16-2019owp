package com.ftn.web.service;

import com.ftn.web.model.LoyaltyCard;
import com.ftn.web.model.User;

public interface LoyaltyCardService {
    void obradi(LoyaltyCard loyaltyCard);
    void uvecajBodove(LoyaltyCard loyaltyCard);
    LoyaltyCard findByUser(User user);
}
