package com.ftn.web.service;

import com.ftn.web.model.KupljenaKnjiga;
import com.ftn.web.model.User;

import java.util.List;

public interface UserService {
    List<User> findAll();
    User findByUserName(String userName);
    void saveUser(User user);
    void updateUser(User user);
    User getUserbyId(int id);
    boolean isUsernameAlreadyInUse(String username);
    void addToCart(KupljenaKnjiga knjiga,User user);
}
