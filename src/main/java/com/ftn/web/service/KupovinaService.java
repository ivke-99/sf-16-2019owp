package com.ftn.web.service;

import com.ftn.web.model.Knjiga;
import com.ftn.web.model.KupljenaKnjiga;
import com.ftn.web.model.Kupovina;
import com.ftn.web.model.User;

import java.util.List;

public interface KupovinaService {
    void zavrsiKupovinu(User user,int brojBodovaZaTrosenje);
    void zavrsiSaGlobalPopust(User user);
    List<Kupovina> korisnikoveKupovine(User user);
    boolean checkIfBought(Knjiga knjiga, User user);
    Kupovina findById(Integer id);
}
