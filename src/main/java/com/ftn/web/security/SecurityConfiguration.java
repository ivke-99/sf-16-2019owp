package com.ftn.web.security;

import com.ftn.web.service.impl.MyUserDetailsService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@EnableWebSecurity
@Configuration
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {


    private final MyUserDetailsService myUserDetailsService;

    public SecurityConfiguration(MyUserDetailsService myUserDetailsService) {
        this.myUserDetailsService = myUserDetailsService;
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(myUserDetailsService);
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
                .antMatchers("/users").hasRole("ADMIN")
                .antMatchers("/saveUser").permitAll()
                .antMatchers("/showDetails/**").hasAnyRole("ADMIN","USER")
                .antMatchers("/showFormForUpdate/**").hasRole("ADMIN")
                .antMatchers("/izmeniKnjigu*").hasRole("ADMIN")
                .antMatchers("/izvestaj").hasRole("ADMIN")
                .antMatchers("/adminLoyaltyCardMenu").hasAnyRole("ADMIN","USER")
                .antMatchers("/dodajPopust*").hasRole("ADMIN")
                .antMatchers("/requestCard").hasRole("USER")
                .antMatchers("/prikaziKupovine").hasRole("USER")
                .antMatchers("/detaljiKupovine/**").hasAnyRole("USER","ADMIN")
                .antMatchers("/adminPrikaziKupovine/**").hasRole("ADMIN")
                .antMatchers("/odobriKarticu/**", "/odbijKarticu/**").hasRole("ADMIN")
                .antMatchers("/poruciKnjigu*").hasRole("ADMIN")
                .antMatchers("/dodajKnjigu").hasRole("ADMIN")
                .antMatchers("/obradiKomentar","/obrisiKomentar/**","/odobriKomentar/**").hasRole("ADMIN")
                .antMatchers("/viewUserProfile").hasRole("USER")
                .antMatchers("/allWishes", "/addWish/**", "/deleteWish/**").hasRole("USER")
                .antMatchers("/korpa").hasRole("USER")
                .antMatchers("/izbrisiIzKorpe/**").hasRole("USER")
                .antMatchers("/dodajKomentar/**").hasRole("USER")
                .antMatchers("/page_success").permitAll()
                .antMatchers("/login*").permitAll()
                .antMatchers("/static/**").permitAll()
                .antMatchers("/assets/**").permitAll()
                .antMatchers("/").permitAll()
                .and().formLogin().loginPage("/login")
                .defaultSuccessUrl("/",true)
                .and().logout().logoutSuccessUrl("/")
                .invalidateHttpSession(false).permitAll();
        http.csrf().disable();
        http.sessionManagement().maximumSessions(3).maxSessionsPreventsLogin(true);
    }

    @Bean
    public PasswordEncoder getPasswordEncoder() {
        return NoOpPasswordEncoder.getInstance();
    }

}
