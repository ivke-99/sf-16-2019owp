package com.ftn.web.util;

import com.ftn.web.model.*;
import com.ftn.web.service.KupljenaKnjigaService;
import com.ftn.web.service.ZanrService;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class DTOConverter {

    private final ZanrService zanrService;

    public DTOConverter(ZanrService zanrService) {
        this.zanrService = zanrService;
    }

    public Knjiga getFromDTO(KnjigaDTO knjigaDTO) {
        Knjiga knjiga = new Knjiga();
        knjiga.setISBN(knjigaDTO.getISBN());
        knjiga.setSlika(knjigaDTO.getSlika());
        knjiga.setPismo(knjigaDTO.getPismo());
        knjiga.setOpis(knjigaDTO.getOpis());
        knjiga.setTip_poveza(knjigaDTO.getTip_poveza());
        knjiga.setNaziv(knjigaDTO.getNaziv());
        knjiga.setJezikKnjige(knjigaDTO.getJezikKnjige());
        knjiga.setProsecnaOcena(knjigaDTO.getProsecnaOcena());
        knjiga.setIzdavacka_kuca(knjigaDTO.getIzdavacka_kuca());
        knjiga.setAutori(knjigaDTO.getAutori());
        knjiga.setGodina_izdavanja(knjigaDTO.getGodina_izdavanja());
        knjiga.setKolicina(knjigaDTO.getKolicina());
        knjiga.setCena(knjigaDTO.getCena());
        knjiga.setBroj_stranica(knjigaDTO.getBroj_stranica());

        List<Zanr> zanroviKnjige = new ArrayList<>();
        for(Integer i : knjigaDTO.getZanrList()) {
            Zanr zanr = zanrService.findByID(i);
            zanroviKnjige.add(zanr);
        }
        knjiga.setZanrList(zanroviKnjige);
        return knjiga;
    }

    public Knjiga getFromDto(KnjigaDTOIzmena knjigaDTOIzmena) {
        Knjiga knjiga = new Knjiga();
        knjiga.setISBN(knjigaDTOIzmena.getISBN());
        knjiga.setSlika(knjigaDTOIzmena.getSlika());
        knjiga.setPismo(knjigaDTOIzmena.getPismo());
        knjiga.setOpis(knjigaDTOIzmena.getOpis());
        knjiga.setTip_poveza(knjigaDTOIzmena.getTip_poveza());
        knjiga.setNaziv(knjigaDTOIzmena.getNaziv());
        knjiga.setJezikKnjige(knjigaDTOIzmena.getJezikKnjige());
        knjiga.setProsecnaOcena(knjigaDTOIzmena.getProsecnaOcena());
        knjiga.setIzdavacka_kuca(knjigaDTOIzmena.getIzdavacka_kuca());
        knjiga.setAutori(knjigaDTOIzmena.getAutori());
        knjiga.setGodina_izdavanja(knjigaDTOIzmena.getGodina_izdavanja());
        knjiga.setKolicina(knjigaDTOIzmena.getKolicina());
        knjiga.setCena(knjigaDTOIzmena.getCena());
        knjiga.setBroj_stranica(knjigaDTOIzmena.getBroj_stranica());

        List<Zanr> zanroviKnjige = new ArrayList<>();
        for(Integer i : knjigaDTOIzmena.getZanrList()) {
            Zanr zanr = zanrService.findByID(i);
            zanroviKnjige.add(zanr);
        }
        knjiga.setZanrList(zanroviKnjige);
        return knjiga;
    }

    public User getFromDTO(UserDTO userDTO) {
        User user = new User();
        user.setUserName(userDTO.getUserName());
        user.setPrezime(userDTO.getPrezime());
        user.setRoles("ROLE_USER");
        user.setPassword(userDTO.getPassword());
        user.setEmail(userDTO.getEmail());
        user.setIme(userDTO.getIme());
        user.setDatum_rodjenja(userDTO.getDatum_rodjenja());
        user.setAdresa(userDTO.getAdresa());
        user.setBr_tel(userDTO.getBr_tel());
        return user;
    }
}
