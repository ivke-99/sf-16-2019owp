package com.ftn.web.util;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Target({ElementType.FIELD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = ISBNValidator.class)
@Documented
public @interface NotExistingISBN {

        String message() default "ISBN already exists";

        Class<?>[] groups() default {};

        Class<? extends Payload>[] payload() default {};

}
