package com.ftn.web.util;

import com.ftn.web.service.GlobalPopustService;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.time.LocalDate;

public class GlobalPopustValidator implements ConstraintValidator<NotExistingDate, LocalDate> {

    private final GlobalPopustService globalPopustService;

    public GlobalPopustValidator(GlobalPopustService globalPopustService) {
        this.globalPopustService = globalPopustService;
    }

    @Override
    public boolean isValid(LocalDate value, ConstraintValidatorContext context) {
        //boolean convention...
        return !globalPopustService.checkIfGlobalPopustExists(value.toString());
    }
}
