package com.ftn.web.util;

import com.ftn.web.service.KnjigaService;
import com.ftn.web.service.UserService;
import org.springframework.stereotype.Component;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

@Component
public class ISBNValidator implements ConstraintValidator<NotExistingISBN, String> {

    private final KnjigaService knjigaService;

    public ISBNValidator(KnjigaService knjigaService) {
        this.knjigaService = knjigaService;
    }

    @Override
    public void initialize(NotExistingISBN constraintAnnotation) {

    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        return knjigaService.isISBNAlreadyInUse(value);
    }
}
