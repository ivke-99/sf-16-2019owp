package com.ftn.web.util;

import com.ftn.web.service.UserService;
import org.springframework.stereotype.Component;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
@Component
public class UsernameValidator implements ConstraintValidator<NotExistingUsername,String> {

    public final UserService userService;
    public UsernameValidator(UserService userService) {
        this.userService = userService;
    }


    public void initialize(NotExistingUsername init) {
    }

    public boolean isValid(String username, ConstraintValidatorContext constraintValidatorContext) {
        return userService.isUsernameAlreadyInUse(username);
    }
}