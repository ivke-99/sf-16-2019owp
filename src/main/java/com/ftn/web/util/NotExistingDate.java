package com.ftn.web.util;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Target({ElementType.FIELD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = GlobalPopustValidator.class)
@Documented
public @interface NotExistingDate {

    String message() default "Popust vec postoji na taj dan.";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
