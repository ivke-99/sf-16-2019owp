package com.ftn.web.repository.impl;

import com.ftn.web.model.User;
import com.ftn.web.model.Wish;
import com.ftn.web.repository.WishRepository;
import com.ftn.web.service.KnjigaService;
import com.ftn.web.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class WishRepositoryImpl implements WishRepository {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private UserService userService;

    @Autowired
    private KnjigaService knjigaService;


    RowMapper<Wish> rowMapper = (rs, rowNum) -> {
        Wish wish = new Wish();
        wish.setId(rs.getInt("id_wish"));
        wish.setKnjiga(knjigaService.findByISBN(rs.getString("book_isbn")));
        wish.setUser(userService.getUserbyId(rs.getInt("id_user")));
        return wish;
    };

    @Override
    public void save(Wish wish) {
        String sql = "insert into wish(book_isbn,id_user) values(?,?)";
        jdbcTemplate.update(sql, wish.getKnjiga().getISBN(), wish.getUser().getId());
    }

    @Override
    public void delete(Wish wish) {
        String sql = "DELETE FROM wish where id_wish = ?";
        jdbcTemplate.update(sql, wish.getId());
    }

    @Override
    public List<Wish> findForUser(User user) {
        String sql = "select * from wish where id_user = ?";
        return jdbcTemplate.query(sql, rowMapper, user.getId());
    }

    @Override
    public Wish findById(int id) {
        return jdbcTemplate.queryForObject("select * from wish where id_wish = ?", rowMapper, id);
    }
}
