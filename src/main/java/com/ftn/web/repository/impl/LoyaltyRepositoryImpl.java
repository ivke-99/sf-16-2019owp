package com.ftn.web.repository.impl;

import com.ftn.web.model.LoyaltyCard;
import com.ftn.web.model.User;
import com.ftn.web.repository.LoyaltyRepository;
import com.ftn.web.repository.UserRepository;
import com.ftn.web.service.UserService;
import lombok.val;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

@Repository
public class LoyaltyRepositoryImpl implements LoyaltyRepository {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private UserService userService;

    @Autowired
    private UserRepository userRepository;

    private class LoyaltyCardRowMapper implements RowMapper<LoyaltyCard> {

        @Override
        public LoyaltyCard mapRow(ResultSet rs, int rowNum) throws SQLException {
            LoyaltyCard loyaltyCard = new LoyaltyCard();
            loyaltyCard.setID(rs.getInt("id"));
            loyaltyCard.setUser(userRepository.getUserById(rs.getInt("id_user")));
            loyaltyCard.setStatus(rs.getString("status"));
            loyaltyCard.setBrojBodova(rs.getInt("br_poena"));
            return loyaltyCard;
        }
    }

    @Override
    public void create(User user) {
        // korisnik je zatrazio karticu
        String sql = "insert into loyalty_card(id_user,br_poena,status) values(?,?,?)";
        jdbcTemplate.update(sql, user.getId(), 0, "cekanje");
    }

    @Override
    public void process(LoyaltyCard kartica) {
        //admin odlucuje da li ce je odbiti ili prihvatiti, u servisnom sloju sredi pa samo ovde zavrsi
        String sql = "update loyalty_card set status = ?, br_poena = ? where id = ?";
        jdbcTemplate.update(sql, kartica.getStatus(), kartica.getBrojBodova(), kartica.getID());
    }

    @Override
    public List<LoyaltyCard> findAll() {
        return jdbcTemplate.query("select * from loyalty_card", new LoyaltyCardRowMapper());
    }

    @Override
    public Optional<LoyaltyCard> findByUser(User user) {
        String sql = "select * from loyalty_card where id_user like ?";
        return Optional.ofNullable(jdbcTemplate.queryForObject(sql, new LoyaltyCardRowMapper(), user.getId()));
    }

    @Override
    public List<LoyaltyCard> findAllForProcessing() {
        String sql = "select * from loyalty_card where status = 'cekanje'";
        return jdbcTemplate.query(sql, new LoyaltyCardRowMapper());
    }

    @Override
    public String isRequested(User user) {
        // da li ce ovo raditi
        String sql = "select status from loyalty_card where id_user like ?";
        try {
            return jdbcTemplate.queryForObject(sql, String.class, user.getId());
        }catch(EmptyResultDataAccessException e) {
            return "nemaZahteva";
        }
    }

    @Override
    public boolean checkIfUserHasLoyaltyCard(User user) {
        try {
            String sql = "select * from loyalty_card where id_user = ? and status like 'prihvacena'";
            jdbcTemplate.queryForObject(sql, new LoyaltyCardRowMapper(), user.getId());
            return true;
        }catch(EmptyResultDataAccessException e){
            return false;
        }
    }

    @Override
    public Integer findPointsForUser(User user) {
        String sql = "select br_poena from loyalty_card where id_user = ?";
        return jdbcTemplate.queryForObject(sql, Integer.class, user.getId());
    }

    @Override
    public void increasePoints(User user, int brPoena) {
        String sql = "update loyalty_card set br_poena = br_poena + ? where id_user = ?";
        jdbcTemplate.update(sql, brPoena, user.getId());
    }

    @Override
    public void decreasePoints(User user, int brPoena) {
        String sql = "update loyalty_card set br_poena = br_poena - ? where id_user = ?";
        jdbcTemplate.update(sql, brPoena, user.getId());
    }
}
