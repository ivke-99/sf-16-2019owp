package com.ftn.web.repository.impl;

import com.ftn.web.model.Knjiga;
import com.ftn.web.model.Zanr;
import com.ftn.web.repository.KnjigaRepository;
import org.springframework.jdbc.core.*;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

@Repository
public class KnjigaRepositoryImpl implements KnjigaRepository {

    private final JdbcTemplate jdbcTemplate;


    public KnjigaRepositoryImpl(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    private class KnjigaRowMapper implements RowMapper<Knjiga> {

        @Override
        public Knjiga mapRow(ResultSet rs, int rowNum) throws SQLException {
            Knjiga knjiga  = new Knjiga();
            knjiga.setISBN(rs.getString("isbn"));
            knjiga.setKolicina(rs.getInt("kolicina"));
            knjiga.setCena(rs.getDouble("cena"));
            knjiga.setAutori(rs.getString("autori"));
            knjiga.setJezikKnjige(rs.getString("jezik_knjige"));
            knjiga.setBroj_stranica(rs.getInt("broj_stranica"));
            knjiga.setGodina_izdavanja(rs.getInt("godina_izdavanja"));
            knjiga.setIzdavacka_kuca(rs.getString("izdavacka_kuca"));
            knjiga.setNaziv(rs.getString("naziv"));
            knjiga.setOpis(rs.getString("opis"));
            knjiga.setPismo(rs.getString("pismo"));
            knjiga.setProsecnaOcena(rs.getDouble("prosecna_ocena"));
            knjiga.setSlika(rs.getString("slika"));
            knjiga.setTip_poveza(rs.getString("tip_poveza"));
            return knjiga;
        }
    }



    private class KnjigaZanrRowCallBackHandler implements RowCallbackHandler {
        private Map<String, Knjiga> knjige = new LinkedHashMap<>();

        @Override
        public void processRow(ResultSet rs) throws SQLException {
            Knjiga knjiga = knjige.get(rs.getString("isbn"));
            if(knjiga == null) {
                knjiga = new Knjiga();
                knjiga.setISBN(rs.getString("isbn"));
                knjiga.setKolicina(rs.getInt("kolicina"));
                knjiga.setCena(rs.getDouble("cena"));
                knjiga.setAutori(rs.getString("autori"));
                knjiga.setJezikKnjige(rs.getString("jezik_knjige"));
                knjiga.setBroj_stranica(rs.getInt("broj_stranica"));
                knjiga.setGodina_izdavanja(rs.getInt("godina_izdavanja"));
                knjiga.setIzdavacka_kuca(rs.getString("izdavacka_kuca"));
                knjiga.setNaziv(rs.getString("naziv"));
                knjiga.setOpis(rs.getString("opis"));
                knjiga.setPismo(rs.getString("pismo"));
                knjiga.setProsecnaOcena(rs.getDouble("prosecna_ocena"));
                knjiga.setSlika(rs.getString("slika"));
                knjiga.setTip_poveza(rs.getString("tip_poveza"));
                knjige.put(knjiga.getISBN(), knjiga);

            }
            Zanr zanr = new Zanr();
            zanr.setId(rs.getInt("id"));
            zanr.setNaziv(rs.getString("naziv"));
            zanr.setOpis(rs.getString("opis"));
            try {
                knjiga.getZanrList().add(zanr);
            }catch(NullPointerException n) {
                knjiga.setZanrList(new ArrayList<>());
                knjiga.getZanrList().add(zanr);
            }
        }

        public List<Knjiga> getKnjige() {
            return new ArrayList<>(knjige.values());
        }
    }

    @Override
    public List<Knjiga> findAll() {
        String sql =
                "SELECT * FROM knjiga k " +
                        "LEFT JOIN knjiga_zanr kz ON kz.knjigaID = k.isbn " +
                        "LEFT JOIN zanr z ON kz.zanrID = z.id " +
                        "ORDER BY k.isbn";

        KnjigaZanrRowCallBackHandler rowCallBackHandler = new KnjigaZanrRowCallBackHandler();
        jdbcTemplate.query(sql,rowCallBackHandler);
        return rowCallBackHandler.getKnjige();
    }

    @Transactional
    @Override
    public void create(Knjiga knjiga) {
        String sqlKnjiga = "INSERT INTO knjiga VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
        jdbcTemplate.update(sqlKnjiga, knjiga.getISBN(), knjiga.getAutori(), knjiga.getBroj_stranica(), knjiga.getCena(),
                knjiga.getGodina_izdavanja(), knjiga.getIzdavacka_kuca(), knjiga.getJezikKnjige(), knjiga.getKolicina(),
                knjiga.getNaziv(), knjiga.getOpis(), knjiga.getPismo(), knjiga.getProsecnaOcena(), knjiga.getSlika(), knjiga.getTip_poveza());
        String sql = "INSERT INTO knjiga_zanr(knjigaID, zanrID) values (?, ?)";
        for(Zanr z : knjiga.getZanrList()) {
            jdbcTemplate.update(sql, knjiga.getISBN(), z.getId());
        }
    }

    @Override
    public Optional<Knjiga> findByISBN(String ISBN) {
        String sql =
                "SELECT * FROM knjiga k " +
                        "LEFT JOIN knjiga_zanr kz ON kz.knjigaID = k.isbn " +
                        "LEFT JOIN zanr z ON kz.zanrID = z.id " +
                        "WHERE k.isbn = ? " +
                        "ORDER BY k.isbn";
        KnjigaZanrRowCallBackHandler rowCallBackHandler = new KnjigaZanrRowCallBackHandler();
        jdbcTemplate.query(sql, rowCallBackHandler, ISBN);
        return Optional.ofNullable(rowCallBackHandler.getKnjige().get(0));
    }

    @Override
    public void update(Knjiga knjiga) {
        String sql = "DELETE FROM knjiga_zanr WHERE knjigaID = ?";
        jdbcTemplate.update(sql, knjiga.getISBN());
        sql = "INSERT INTO knjiga_zanr (knjigaID, zanrId) VALUES (?, ?)";
        for (Zanr itZanr: knjiga.getZanrList()) {
            jdbcTemplate.update(sql, knjiga.getISBN(), itZanr.getId());
        }

        sql = "UPDATE knjiga SET autori = ?, broj_stranica = ?, cena = ?, godina_izdavanja = ?, izdavacka_kuca = ?, jezik_knjige = ?" +
                ",naziv = ?, opis = ?, pismo = ?, slika = ?, tip_poveza = ? WHERE isbn = ?";
        jdbcTemplate.update(sql, knjiga.getAutori(),knjiga.getBroj_stranica(),knjiga.getCena(),knjiga.getGodina_izdavanja(),
                knjiga.getIzdavacka_kuca(),knjiga.getJezikKnjige(),knjiga.getNaziv(),knjiga.getOpis(),knjiga.getPismo(),
                knjiga.getSlika(),knjiga.getTip_poveza(), knjiga.getISBN());
    }

    @Override
    public List<Knjiga> searchByParams(String naziv, String autori, String jezik_knjige, Double min_cena, Double max_cena, String ISBN) {
        String sql ="select * from knjiga k where k.naziv like ? and k.autori like ? and k.jezik_knjige like ? and cena > ? and cena < ? and isbn like ?";
        String nazivFormatted = "%" + naziv + "%";
        String autoriFormatted = "%" + autori + "%";
        String jezikFormatted = "%" + jezik_knjige + "%";
        String isbnforatted = "%" + ISBN + "%";
        return jdbcTemplate.query(sql, new KnjigaRowMapper(), nazivFormatted,autoriFormatted,jezikFormatted,min_cena,max_cena,isbnforatted);
    }

    @Override
    public List<String> findAllISBN()
    {
        return jdbcTemplate.query("SELECT isbn from knjiga", new BeanPropertyRowMapper<>(String.class));
    }

    @Override
    public void poruciKnjigu(Knjiga knjiga) {
        String sql = "update knjiga set kolicina = ? where isbn = ?";
        jdbcTemplate.update(sql,knjiga.getKolicina(),knjiga.getISBN());
    }

    @Override
    public void obradaOcene(Knjiga knjiga) {
        String sql = "update knjiga set prosecna_ocena = ? where isbn = ?";
        jdbcTemplate.update(sql, knjiga.getProsecnaOcena(), knjiga.getISBN());
    }
}
