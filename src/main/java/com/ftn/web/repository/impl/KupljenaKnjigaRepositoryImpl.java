package com.ftn.web.repository.impl;

import com.ftn.web.model.Knjiga;
import com.ftn.web.model.KupljenaKnjiga;
import com.ftn.web.model.Kupovina;
import com.ftn.web.repository.KnjigaRepository;
import com.ftn.web.repository.KupljenaKnjigaRepository;
import com.ftn.web.service.KnjigaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

@Repository
public class KupljenaKnjigaRepositoryImpl implements KupljenaKnjigaRepository {

    private final JdbcTemplate jdbcTemplate;

    @Autowired
    private KnjigaService knjigaService;


    public KupljenaKnjigaRepositoryImpl(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    private class KupljenaKnjigaRowMapper implements RowMapper<KupljenaKnjiga> {

        @Override
        public KupljenaKnjiga mapRow(ResultSet rs, int rowNum) throws SQLException {
                KupljenaKnjiga kupljenaKnjiga = new KupljenaKnjiga();
                kupljenaKnjiga.setID(rs.getInt("id"));
                kupljenaKnjiga.setCena(rs.getDouble("cena"));
                kupljenaKnjiga.setBrojPrimeraka(rs.getInt("broj_primeraka"));
                kupljenaKnjiga.setKnjiga(knjigaService.findByISBN(rs.getString("knjiga_isbn")));
                return kupljenaKnjiga;
        }
    }


    @Override
    public List<KupljenaKnjiga> findAll()
    {
        String sql= "select * from kupljena_knjiga kk inner join knjiga k on kk.knjiga_isbn = k.isbn order by knjiga_isbn"; // proveri ovo
        return jdbcTemplate.query(sql, new KupljenaKnjigaRowMapper());
    }

    @Override
    public Optional<KupljenaKnjiga> findById(int id) {
        String sql =  "select * from kupljena_knjiga kk inner join knjiga k on kk.knjiga_isbn = k.isbn where knjiga_isbn = ? order by knjiga_isbn";
        return Optional.ofNullable(jdbcTemplate.queryForObject(sql, new KupljenaKnjigaRowMapper(), id));
    }

    @Override
    public void save(KupljenaKnjiga kupljenaKnjiga) {
        String sql = "insert into kupljena_knjiga(id,broj_primeraka,cena,knjiga_isbn) values (?,?,?)";
        jdbcTemplate.update(sql,kupljenaKnjiga.getID(),kupljenaKnjiga.getBrojPrimeraka(),kupljenaKnjiga.getCena(),kupljenaKnjiga.getKnjiga().getISBN());
    }
}
