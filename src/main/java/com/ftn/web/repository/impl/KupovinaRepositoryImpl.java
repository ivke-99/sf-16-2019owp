package com.ftn.web.repository.impl;

import com.ftn.web.model.*;
import com.ftn.web.repository.KupovinaRepository;
import com.ftn.web.repository.UserRepository;
import com.ftn.web.service.KnjigaService;
import com.ftn.web.service.KupljenaKnjigaService;
import com.ftn.web.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.*;

@Repository
public class KupovinaRepositoryImpl implements KupovinaRepository {

    @Autowired
    private KnjigaService knjigaService;

    @Autowired
    private KupljenaKnjigaService kupljenaKnjigaService;

    @Autowired
    private UserService userService;

    @Autowired
    private UserRepository userRepository;

    private final JdbcTemplate jdbcTemplate;

    public KupovinaRepositoryImpl(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    private class KupovinaKupljenaRowCallBackHandler implements RowCallbackHandler {
        private Map<Integer, Kupovina> kupovine = new LinkedHashMap<>();

        @Override
        public void processRow(ResultSet rs) throws SQLException {
            Kupovina kupovina = kupovine.get(rs.getInt("id"));
            if(kupovina == null) {
                kupovina = new Kupovina();
                kupovina.setIdKupovine(rs.getInt("id"));
                kupovina.setDatumKupovine(rs.getDate("datum_kupovine").toLocalDate());
                kupovina.setUkupnaCena(rs.getDouble("ukupna_cena"));
                kupovina.setKorisnik(userRepository.getUserById(rs.getInt("user_id")));
                kupovine.put(kupovina.getIdKupovine(), kupovina);

            }
            KupljenaKnjiga kupljenaKnjiga = new KupljenaKnjiga();
            kupljenaKnjiga.setID(rs.getInt("id"));
            kupljenaKnjiga.setCena(rs.getDouble("cena"));
            kupljenaKnjiga.setBrojPrimeraka(rs.getInt("broj_primeraka"));
            kupljenaKnjiga.setKnjiga(knjigaService.findByISBN(rs.getString("knjiga_isbn")));
            try {
                kupovina.getKupljenaKnjigaList().add(kupljenaKnjiga);
            }catch(NullPointerException n) {
                kupovina.setKupljenaKnjigaList(new ArrayList<>());
                kupovina.getKupljenaKnjigaList().add(kupljenaKnjiga);
            }
        }

        public List<Kupovina> getKupovine() {
            return new ArrayList<>(kupovine.values());
        }
    }

    private final String sql_forall = "select * from kupovina\n" +
            "                left join user on kupovina.user_id = user.id\n" +
            "                left join kupovina_knjige on kupovina.id = kupovina_knjige.kupovinaID\n" +
            "                left join kupljena_knjiga on kupljena_knjiga.id = kupovina_knjige.kupljenaID\n" +
            "                left join knjiga on knjiga.isbn = kupljena_knjiga.knjiga_isbn\n" +
            "                order by kupovina.id";

    @Override
    public List<Kupovina> findAll() {
        KupovinaKupljenaRowCallBackHandler handler = new KupovinaKupljenaRowCallBackHandler();
        jdbcTemplate.query(sql_forall, handler);
        return handler.getKupovine();
    }

    @Override
    public Optional<Kupovina> findById(int id) {
        KupovinaKupljenaRowCallBackHandler handler = new KupovinaKupljenaRowCallBackHandler();
        String sql = "select * from kupovina\n" +
                "                left join user on kupovina.user_id = user.id\n" +
                "                left join kupovina_knjige on kupovina.id = kupovina_knjige.kupovinaID\n" +
                "                left join kupljena_knjiga on kupljena_knjiga.id = kupovina_knjige.kupljenaID\n" +
                "                left join knjiga on knjiga.isbn = kupljena_knjiga.knjiga_isbn\n" +
                "                where kupovina.id = ? " +
                "                order by kupovina.id";
        jdbcTemplate.query(sql, handler, id);
        return Optional.ofNullable(handler.getKupovine().get(0));
    }

    @Transactional
    @Override
    public void save(Kupovina kupovina) {
        String sql1 = "insert into kupovina(ukupna_cena,datum_kupovine,user_id) values (?,?,?)";
        jdbcTemplate.update(sql1, kupovina.getUkupnaCena(),kupovina.getDatumKupovine(),kupovina.getKorisnik().getId());
        String sql2 = "insert into kupljena_knjiga(broj_primeraka,cena,knjiga_isbn) values (?,?,?)";
        for(KupljenaKnjiga k : kupovina.getKupljenaKnjigaList()) {
            jdbcTemplate.update(sql2, k.getBrojPrimeraka(),k.getCena(),k.getKnjiga().getISBN());
        }
        String sql3 = "insert into kupovina_knjige values(?,?)";
        for(KupljenaKnjiga k : kupovina.getKupljenaKnjigaList()) {
            jdbcTemplate.update(sql3, k.getID(), kupovina.getIdKupovine());
        }

    }

    @Override
    public List<Kupovina> getUserPurchases(User user) {
        KupovinaKupljenaRowCallBackHandler kupovinaKupljenaRowCallBackHandler = new KupovinaKupljenaRowCallBackHandler();
        String sql_User = "select * from kupovina\n" +
                "                left join user on kupovina.user_id = user.id\n" +
                "                left join kupovina_knjige on kupovina.id = kupovina_knjige.kupovinaID\n" +
                "                left join kupljena_knjiga on kupljena_knjiga.id = kupovina_knjige.kupljenaID\n" +
                "                left join knjiga on knjiga.isbn = kupljena_knjiga.knjiga_isbn\n" +
                "                where kupovina.user_id = ?\n" +
                "                group by kupovina.id, kupovina.datum_kupovine\n" +
                "                order by kupovina.datum_kupovine desc";
        jdbcTemplate.query(sql_User, kupovinaKupljenaRowCallBackHandler, user.getId());
        return kupovinaKupljenaRowCallBackHandler.getKupovine();
    }




    private class IzvestajMapper implements RowMapper<Izvestaj> {

        @Override
        public Izvestaj mapRow(ResultSet rs, int rowNum) throws SQLException {
            Izvestaj izvestaj = new Izvestaj();
            izvestaj.setIsbn(rs.getString("isbn"));
            izvestaj.setAutori(rs.getString("autori"));
            izvestaj.setUkupanBroj(rs.getInt("UkupanBroj"));
            izvestaj.setUkupnaCena(rs.getDouble("UkupnaCena"));
            izvestaj.setNaziv(rs.getString("naziv"));
            return izvestaj;
        }
    }

    @Override
    public List<Izvestaj> findByParams(String startDate, String endDate) {
        String sql = "select knjiga.isbn,knjiga.naziv,knjiga.autori,sum(kupljena_knjiga.broj_primeraka) as UkupanBroj,sum(broj_primeraka * knjiga.cena) as UkupnaCena from kupljena_knjiga \n" +
                "inner join knjiga on kupljena_knjiga.knjiga_isbn = knjiga.isbn \n" +
                "inner join kupovina_knjige on kupovina_knjige.kupljenaID = kupljena_knjiga.id \n" +
                "inner join kupovina on kupovina.id = kupovina_knjige.kupovinaID \n" +
                "where kupovina.datum_kupovine between ? and ? \n" +
                "group by isbn,naziv,autori";

        return jdbcTemplate.query(sql, new IzvestajMapper(), startDate, endDate);
    }
}
