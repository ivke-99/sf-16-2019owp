package com.ftn.web.repository.impl;

import com.ftn.web.model.Zanr;
import com.ftn.web.repository.ZanrRepository;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public class ZanrRepositoryImpl implements ZanrRepository {

    private final JdbcTemplate jdbcTemplate;

    RowMapper<Zanr> rowMapper = (rs, rowNum) -> {
        Zanr zanr = new Zanr();
        zanr.setId(rs.getInt("id"));
        zanr.setNaziv(rs.getString("naziv"));
        zanr.setOpis(rs.getString("opis"));
        return zanr;
    };

    public ZanrRepositoryImpl(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public List<Zanr> findAll() {
        return jdbcTemplate.query("select * from zanr", new BeanPropertyRowMapper<>(Zanr.class));
    }

    @Override
    public Optional<Zanr> findById(int id) {
        return Optional.ofNullable(jdbcTemplate.queryForObject("select * from zanr where id = ?", rowMapper, id));
    }
}
