package com.ftn.web.repository.impl;

import com.ftn.web.model.User;
import com.ftn.web.repository.UserRepository;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Repository
public class UserRepositoryImpl implements UserRepository {

    private final JdbcTemplate jdbcTemplate;
    private final NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    RowMapper<User> rowMapper = (rs, rowNum) -> {
        User user = new User();
        user.setId(rs.getInt("id"));
        user.setActive(rs.getBoolean("active"));
        user.setAdresa(rs.getString("adresa"));
        user.setBr_tel(rs.getString("br_tel"));
        user.setDatum_rodjenja(rs.getDate("datum_rodjenja").toLocalDate());
        user.setDatumvreme_registracije(rs.getTimestamp("datumvreme_registracije").toLocalDateTime());
        user.setEmail(rs.getString("email"));
        user.setIme(rs.getString("ime"));
        user.setPassword(rs.getString("password"));
        user.setUserName(rs.getString("user_name"));
        user.setPrezime(rs.getString("prezime"));
        user.setRoles(rs.getString("roles"));
        return user;
    };

    public UserRepositoryImpl(JdbcTemplate jdbcTemplate, NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
        this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
    }


    @Override
    public List<User> findAll() {
        return jdbcTemplate.query("select * from user", new BeanPropertyRowMapper<>(User.class));
    }

    @Override
    public void save(User user) {
        String sql = "insert into user(active,adresa,br_tel,datum_rodjenja,datumvreme_registracije,email,ime,password,prezime,roles,user_name)" +
                "values(?,?,?,?,?,?,?,?,?,?,?)";
        jdbcTemplate.update(sql, true,user.getAdresa(),user.getBr_tel(),user.getDatum_rodjenja(), LocalDateTime.now(), user.getEmail(),user.getIme()
                ,user.getPassword(),user.getPrezime(),user.getRoles(),user.getUserName());
    }

    @Override
    public Optional<User> findByUserName(String username) {
        String sql = "select * from user where user_name = ?";
        return Optional.ofNullable(jdbcTemplate.queryForObject(sql, rowMapper, username));
    }

    @Override
    public User getUserById(int id) {
        String sql = "select * from user where id = ?";
        return jdbcTemplate.queryForObject(sql, rowMapper, id);
    }

    @Override
    public void update(User user) {
        String sql = "update user set active = ?, roles = ? where user_name = ?";
        jdbcTemplate.update(sql, user.isActive(),user.getRoles(),user.getUserName());
    }

    @Override
    public void disable(User user) {
        String sql = "update user set active = ? where user_name = ?";
        jdbcTemplate.update(sql,false, user.getUserName());
    }
}
