package com.ftn.web.repository.impl;

import com.ftn.web.model.Komentar;
import com.ftn.web.repository.KnjigaRepository;
import com.ftn.web.repository.KomentarRepository;
import com.ftn.web.repository.KupovinaRepository;
import com.ftn.web.repository.UserRepository;
import com.ftn.web.service.KnjigaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

@Repository
public class KomentarRepositoryImpl implements KomentarRepository {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private KnjigaRepository knjigaRepository;

    @Autowired
    private KupovinaRepository kupovinaRepository;

    @Autowired
    private KnjigaService knjigaService;

    private class KomentarRowMapper implements RowMapper<Komentar> {

        @Override
        public Komentar mapRow(ResultSet rs, int rowNum) throws SQLException {
            Komentar komentar = new Komentar();
            komentar.setID(rs.getInt("id_kom"));
            komentar.setKorisnik(userRepository.getUserById(rs.getInt("id_user")));
            komentar.setKomentar(rs.getString("opis"));
            komentar.setKnjiga(knjigaService.findByISBN(rs.getString("ISBN_knjige")));
            komentar.setStatus(rs.getString("status"));
            komentar.setOcena(rs.getInt("ocena_korisnika"));
            return komentar;
        }
    }


    @Override
    public List<Komentar> findAll() {
        String sql = "select * from komentar k\n" +
                "left join user u on k.id_user = u.id\n" +
                "left join knjiga kn on k.ISBN_knjige = kn.isbn\n" +
                "order by id_kom";
        return jdbcTemplate.query(sql, new KomentarRowMapper());
    }

    @Override
    public Optional<Komentar> findById(int id) {
        String sql = "select * from komentar k \n" +
                "left join user u on k.id_user = u.id \n" +
                "left join knjiga kn on k.ISBN_knjige = kn.isbn \n" +
                "where k.id_kom = ?";
        return Optional.ofNullable(jdbcTemplate.queryForObject(sql, new KomentarRowMapper(), id));
    }


    @Override
    public void save(Komentar komentar) {
        String sql1 = "insert into komentar(id_user,ISBN_knjige,ocena_korisnika,status,opis) values(?,?,?,?,?)";
        jdbcTemplate.update(sql1, komentar.getKorisnik().getId(), komentar.getKnjiga().getISBN(), komentar.getOcena(),komentar.getStatus(),komentar.getKomentar());
    }

    @Override
    public void process(Komentar komentar) {
        String sql = "update komentar set status = ? where id_kom = ?";
        jdbcTemplate.update(sql,komentar.getStatus(),komentar.getID());
        if(komentar.getStatus().equals("prihvacen")) {
            //ova logika za sada ide ovde
            String sql2 = "select ocena_korisnika from komentar where status like 'prihvacen' and ISBN_knjige like " + komentar.getKnjiga().getISBN();
            List<Double> sveOcene = jdbcTemplate.queryForList(sql2, Double.class);

            //menjamo ocenu u knjizi
            double noviProsek =  sveOcene.stream().mapToDouble(i-> i).average().orElse(0);
            komentar.getKnjiga().setProsecnaOcena(noviProsek);
            knjigaRepository.obradaOcene(komentar.getKnjiga());
        }
    }
}
