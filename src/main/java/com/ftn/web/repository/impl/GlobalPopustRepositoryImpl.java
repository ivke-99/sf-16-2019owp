package com.ftn.web.repository.impl;

import com.ftn.web.model.GlobalPopust;
import com.ftn.web.repository.GlobalPopustRepository;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Optional;

@Repository
public class GlobalPopustRepositoryImpl implements GlobalPopustRepository {

    private final JdbcTemplate jdbcTemplate;

    public GlobalPopustRepositoryImpl(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    private class PopustRowMapper implements RowMapper<GlobalPopust> {

        @Override
        public GlobalPopust mapRow(ResultSet rs, int rowNum) throws SQLException {
            GlobalPopust g = new GlobalPopust();
            g.setDanPopusta(rs.getDate("datum").toLocalDate());
            g.setPopust(rs.getFloat("popust"));
            return g;
        }
    }

    @Override
    public void save(GlobalPopust globalPopust) {
        String sql = "insert into special_popust(datum,popust) values(?,?)";
        jdbcTemplate.update(sql, globalPopust.getDanPopusta(), globalPopust.getPopust());
    }

    @Override
    public GlobalPopust findOne(String date) {
        String sql = "select * from special_popust where datum = ?";
        try {
            return jdbcTemplate.queryForObject(sql, new PopustRowMapper(), date);
        }catch(EmptyResultDataAccessException e) {
            return null;
        }
    }
}
