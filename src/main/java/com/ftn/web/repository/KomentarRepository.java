package com.ftn.web.repository;

import com.ftn.web.model.Komentar;

import java.util.List;
import java.util.Optional;

public interface KomentarRepository {
    List<Komentar> findAll();
    Optional<Komentar> findById(int id);
    void save(Komentar komentar);
    void process(Komentar komentar);
}
