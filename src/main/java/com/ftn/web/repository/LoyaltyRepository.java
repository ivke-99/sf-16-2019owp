package com.ftn.web.repository;

import com.ftn.web.model.LoyaltyCard;
import com.ftn.web.model.User;

import java.util.List;
import java.util.Optional;

public interface LoyaltyRepository {
    void create(User kartica);
    void process(LoyaltyCard kartica);
    List<LoyaltyCard> findAll();
    Optional<LoyaltyCard> findByUser(User user);
    List<LoyaltyCard> findAllForProcessing();
    String isRequested(User user);
    boolean checkIfUserHasLoyaltyCard(User user);
    Integer findPointsForUser(User user);
    void increasePoints(User user, int brPoena);
    void decreasePoints(User user, int brPoena);
}
