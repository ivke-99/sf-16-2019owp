package com.ftn.web.repository;

import com.ftn.web.model.User;
import com.ftn.web.model.Wish;

import java.util.List;

public interface WishRepository {
    void save(Wish wish);
    void delete(Wish wish);
    List<Wish> findForUser(User user);
    Wish findById(int id);
}
