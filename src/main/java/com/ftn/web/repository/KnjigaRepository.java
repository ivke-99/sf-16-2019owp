package com.ftn.web.repository;

import com.ftn.web.model.Knjiga;

import java.util.List;
import java.util.Optional;


public interface KnjigaRepository {
    List<Knjiga> findAll();
    void create(Knjiga knjiga);
    Optional<Knjiga> findByISBN(String ISBN);
    void update(Knjiga knjiga);
    List<Knjiga> searchByParams(String naziv,String autori,String jezik_knjige,Double min_cena,Double max_cena, String ISBN);
    List<String> findAllISBN();
    void poruciKnjigu(Knjiga knjiga);
    void obradaOcene(Knjiga knjiga);
}
