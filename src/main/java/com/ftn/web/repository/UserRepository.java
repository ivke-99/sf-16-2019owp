package com.ftn.web.repository;

import com.ftn.web.model.User;
import java.util.List;
import java.util.Optional;

public interface UserRepository{
    List<User> findAll();
    void save(User user);
    Optional<User> findByUserName(String username);
    User getUserById(int id);
    void update(User user);
    void disable(User user);
}
