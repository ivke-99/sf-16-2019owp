package com.ftn.web.repository;

import com.ftn.web.model.GlobalPopust;

import java.util.Optional;

public interface GlobalPopustRepository {
    void save(GlobalPopust globalPopust);
    GlobalPopust findOne(String date);
}
