package com.ftn.web.repository;

import com.ftn.web.model.Zanr;
import java.util.List;
import java.util.Optional;


public interface ZanrRepository{
    List<Zanr> findAll();
    Optional<Zanr> findById(int id);
}
