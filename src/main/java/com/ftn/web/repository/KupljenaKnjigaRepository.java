package com.ftn.web.repository;

import com.ftn.web.model.KupljenaKnjiga;

import java.util.List;
import java.util.Optional;


public interface KupljenaKnjigaRepository {
    List<KupljenaKnjiga> findAll();
    Optional<KupljenaKnjiga> findById(int id);
    void save(KupljenaKnjiga kupljenaKnjiga);
}
