package com.ftn.web.repository;

import com.ftn.web.model.Izvestaj;
import com.ftn.web.model.Kupovina;
import com.ftn.web.model.User;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;


public interface KupovinaRepository {
    List<Kupovina> findAll();
    Optional<Kupovina> findById(int id);
    void save(Kupovina kupovina);
    List<Izvestaj> findByParams(String startDate, String endDate);
    List<Kupovina> getUserPurchases(User user);
}
