package com.ftn.web.controller;

import com.ftn.web.model.Knjiga;
import com.ftn.web.model.User;
import com.ftn.web.model.Wish;
import com.ftn.web.repository.WishRepository;
import com.ftn.web.service.KnjigaService;
import com.ftn.web.service.UserService;
import lombok.val;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class WishController {

    private final WishRepository wishRepository;
    private final UserService userService;
    private final KnjigaService knjigaService;

    public WishController(WishRepository wishRepository, UserService userService, KnjigaService knjigaService) {
        this.wishRepository = wishRepository;
        this.userService = userService;
        this.knjigaService = knjigaService;
    }

    @PostMapping("/addWish/{isbn}")
    public String addWish(@PathVariable("isbn") String isbn,Authentication auth) {
        User user = userService.findByUserName(auth.getName());
        Knjiga knjiga = knjigaService.findByISBN(isbn);
        val allWishes = wishRepository.findForUser(user);
        Wish wish = new Wish();
        wish.setUser(user);
        wish.setKnjiga(knjiga);
        if(allWishes.stream().anyMatch(e -> e.getKnjiga().getISBN().equals(wish.getKnjiga().getISBN()) && e.getUser().getId() == wish.getUser().getId() )) {
            return "redirect:/allWishes";
        }
        else {
            wishRepository.save(wish);
            return "redirect:/";
        }
    }

    @PostMapping("/deleteWish/{id_wish}")
    public String deleteWish(@PathVariable("id_wish") int wishID) {
        wishRepository.delete(wishRepository.findById(wishID));
        return "redirect:/allWishes";
    }

    @GetMapping("/allWishes")
    public String getUserWishes(Authentication auth, Model model) {
        User user = userService.findByUserName(auth.getName());
        val allWishes = wishRepository.findForUser(user);
        model.addAttribute("allWishes", allWishes);
        return "user_wishes";
    }
}
