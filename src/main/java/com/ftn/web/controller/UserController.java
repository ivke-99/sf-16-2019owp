package com.ftn.web.controller;

import com.ftn.web.model.User;
import com.ftn.web.model.UserDTO;
import com.ftn.web.service.UserService;
import com.ftn.web.util.DTOConverter;
import lombok.val;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Controller
public class UserController {

    private final UserService userService;
    private final DTOConverter dtoConverter;

    public UserController(UserService userService, DTOConverter dtoConverter) {
        this.userService = userService;
        this.dtoConverter = dtoConverter;
    }

    // display all users
    @GetMapping("/users")
    public String viewAllUsers(Model model){
        model.addAttribute("listUsers", userService.findAll());
        return "users";
    }

    @PostMapping("/saveUser")
    public String saveUser(@Valid @ModelAttribute("user") UserDTO user, BindingResult bindingResult) {
        if(bindingResult.hasErrors()) {
            return "new_user";
        }
        else {
            userService.saveUser(dtoConverter.getFromDTO(user));
            return "page_success";
        }
    }


    @GetMapping("/createUserForm")
    public String createUserForm(Model model) {
        val allUsers = userService.findAll();
        User user = new User();
        model.addAttribute("user", user);
        model.addAttribute("allUsers", allUsers);
        return "new_user";
    }

    @PostMapping("/updateUser")
    public String updateUser(@ModelAttribute("user") User user) {
        userService.updateUser(user);
        return "page_success";
    }


    @GetMapping("/showFormForUpdate/{id}")
    public String showFormForUpdate(@PathVariable(value = "id") int id, Model model){
        User user = userService.getUserbyId(id);
        model.addAttribute("user", user);
        return "update_user";
    }

    @GetMapping("/viewUserProfile")
    public String viewUserProfile(Authentication auth,Model model) {
        User user = userService.findByUserName(auth.getName());
        model.addAttribute("user", user);
        return "update_user";
    }
}
