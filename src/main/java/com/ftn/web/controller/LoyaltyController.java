package com.ftn.web.controller;

import com.ftn.web.model.LoyaltyCard;
import com.ftn.web.model.User;
import com.ftn.web.repository.LoyaltyRepository;
import com.ftn.web.repository.UserRepository;
import com.ftn.web.service.LoyaltyCardService;
import com.ftn.web.service.UserService;
import lombok.val;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class LoyaltyController {

    private final LoyaltyRepository loyaltyRepository;
    private final UserRepository userRepository;
    private final UserService userService;
    private final LoyaltyCardService loyaltyCardService;

    public LoyaltyController(LoyaltyRepository loyaltyRepository, UserRepository userRepository, UserService userService, LoyaltyCardService loyaltyCardService) {
        this.loyaltyRepository = loyaltyRepository;
        this.userRepository = userRepository;
        this.userService = userService;
        this.loyaltyCardService = loyaltyCardService;
    }

    @PostMapping("/odobriKarticu/{userName}")
    public String odobriKarticu(@PathVariable("userName") String userName,Model model) {
        LoyaltyCard loyaltyCard = loyaltyCardService.findByUser(userService.findByUserName(userName));
        loyaltyCard.setStatus("prihvacena");
        loyaltyCardService.obradi(loyaltyCard);
        return "page_success";
    }

    @PostMapping("/odbijKarticu/{userName}")
    public String odbijKarticu(@PathVariable("userName") String userName,Model model) {
        LoyaltyCard loyaltyCard = loyaltyCardService.findByUser(userService.findByUserName(userName));
        loyaltyCard.setStatus("odbijena");
        loyaltyCardService.obradi(loyaltyCard);
        return "page_success";
    }

    @GetMapping("/adminLoyaltyCardMenu")
    public String adminCardMenu(Model model) {
        val sveKartice = loyaltyRepository.findAllForProcessing();
        model.addAttribute("allCards", sveKartice);
        return "loyaltyCardMenu";
    }

    @PostMapping("/requestCard")
    public String requestCard(Authentication auth) {
        User user = userService.findByUserName(auth.getName());
        loyaltyRepository.create(user);
        return "page_success";
    }

    @GetMapping("/userLoyaltyCard")
    public String userLoyaltyCardMenu(Model model, Authentication auth) {
        User user = userService.findByUserName(auth.getName());
        String isRequested = loyaltyRepository.isRequested(user);
        model.addAttribute("status", isRequested);
        return "loyaltyCardMenu";
    }
}
