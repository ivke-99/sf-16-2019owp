package com.ftn.web.controller;

import com.ftn.web.model.*;
import com.ftn.web.repository.GlobalPopustRepository;
import com.ftn.web.repository.KupovinaRepository;
import com.ftn.web.repository.LoyaltyRepository;
import com.ftn.web.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.time.LocalDate;
import java.util.ArrayList;

@Controller
public class KorisnickaKorpaController {


    @Autowired
    KorisnickaKorpa korisnickaKorpa;

    private final KupljenaKnjigaService kupljenaKnjigaService;
    private final UserService userService;
    private final KupovinaService kupovinaService;
    private final KnjigaService knjigaService;
    private final KupovinaRepository kupovinaRepository;
    private final LoyaltyRepository loyaltyRepository;
    private final GlobalPopustRepository globalPopustRepository;
    private final GlobalPopustService globalPopustService;

    public KorisnickaKorpaController(KupljenaKnjigaService kupljenaKnjigaService, UserService userService, KupovinaService kupovinaService, KnjigaService knjigaService, KupovinaRepository kupovinaRepository, LoyaltyRepository loyaltyRepository, GlobalPopustRepository globalPopustRepository, GlobalPopustService globalPopustService) {
        this.kupljenaKnjigaService = kupljenaKnjigaService;
        this.userService = userService;
        this.kupovinaService = kupovinaService;
        this.knjigaService = knjigaService;
        this.kupovinaRepository = kupovinaRepository;
        this.loyaltyRepository = loyaltyRepository;
        this.globalPopustRepository = globalPopustRepository;
        this.globalPopustService = globalPopustService;
    }

    @PostMapping("/dodajUKorpu")
    public String dodajKnjiguUKorpu(@RequestParam("ISBN") String ISBN,
                                    @RequestParam(value = "brojPrimeraka", required = false) int brojPrimeraka) {
        Knjiga knjiga = knjigaService.findByISBN(ISBN);
        KupljenaKnjiga kupljenaKnjiga = kupljenaKnjigaService.kreirajZaKupovinu(knjiga,brojPrimeraka);
        korisnickaKorpa.dodajUKorpu(kupljenaKnjiga);
        return "page_success";
    }

    @PostMapping("/izbrisiIzKorpe/{ID}")
    public String izbrisiIzKorpe(@PathVariable("ID") int ID, Authentication authentication) {
        User user = userService.findByUserName(authentication.getName());
        KupljenaKnjiga knjigaZaBrisanje = korisnickaKorpa.getById(ID);
        korisnickaKorpa.removeFromCart(knjigaZaBrisanje);
        return "redirect:/korpa";
    }


    @GetMapping("/korpa")
    public String korisnickaKorpa(Model model){
        ArrayList<KupljenaKnjiga> listaKorisnickihKnjiga = new ArrayList<>(korisnickaKorpa.getKupljenaKnjigaList());
        model.addAttribute("listaKnjiga", listaKorisnickihKnjiga);
        Double ukupnaCena = korisnickaKorpa.getKupljenaKnjigaList().stream().mapToDouble(KupljenaKnjiga::getCena).sum();
        model.addAttribute("ukupnaCena", ukupnaCena);
        return "korisnickaKorpa";
    }


    @PostMapping("/iskoristiBodove")
    public String iskoristiBodove(@RequestParam("bodoviZaTrosenje") int bodovi,Authentication auth) {
        User user = userService.findByUserName(auth.getName());
        kupovinaService.zavrsiKupovinu(user, bodovi);
        return "redirect:/";
    }

    @GetMapping("/unosBodova")
    public String korisnickaObradaBodova(Authentication authentication,Model model) {
        User user = userService.findByUserName(authentication.getName());
        int bodovi = loyaltyRepository.findPointsForUser(user);
        model.addAttribute("bodovi", bodovi);
        return "kupovinaBodovi";
    }

    @PostMapping("/zavrsiKupovinu")
    public String zavrsiKupovinu(Authentication authentication) {
        User user = userService.findByUserName(authentication.getName());
        boolean hasCard = loyaltyRepository.checkIfUserHasLoyaltyCard(user);
        boolean specialDate = globalPopustService.checkIfTodayIsSpecial();
        if(specialDate) {
            kupovinaService.zavrsiSaGlobalPopust(user);
            return "redirect:/";
        }
        if(hasCard) {
            return "redirect:/unosBodova";
        }
        try {
            kupovinaService.zavrsiKupovinu(user,0);
            return "page_success";
        }catch (Exception e) {
            return "redirect:/";
        }
    }
}
