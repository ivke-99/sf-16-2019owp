package com.ftn.web.controller;

import com.ftn.web.model.Izvestaj;
import com.ftn.web.repository.KupovinaRepository;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.time.LocalDate;
import java.util.List;

@Controller
@RequestMapping("/izvestaj")
public class IzvestavanjeController {

    private final KupovinaRepository kupovinaRepository;

    public IzvestavanjeController(KupovinaRepository kupovinaRepository) {
        this.kupovinaRepository = kupovinaRepository;
    }

    @PostMapping
    public String izvestavanje(@RequestParam("startDate") String date1,
                               @RequestParam("endDate") String date2, Model model) {
        List<Izvestaj> izvestajList = kupovinaRepository.findByParams(date1,date2);
        Double ukupnaCenaSvega = izvestajList.stream().mapToDouble(Izvestaj::getUkupnaCena).sum();
        Integer ukupnaProdataKolicina = izvestajList.stream().mapToInt(Izvestaj::getUkupanBroj).sum();
        model.addAttribute("izvestajList", izvestajList);
        model.addAttribute("ukupnaCenaKnjiga", ukupnaCenaSvega);
        model.addAttribute("ukupanBrojKnjiga", ukupnaProdataKolicina);
        return "izvestajAdmin";
    }

    @GetMapping
    public String izvestaj() {
        return "izvestajAdmin";
    }
}
