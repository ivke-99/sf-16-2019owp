package com.ftn.web.controller;

import com.ftn.web.model.Komentar;
import com.ftn.web.model.User;
import com.ftn.web.repository.KomentarRepository;
import com.ftn.web.service.KnjigaService;
import com.ftn.web.service.KomentarService;
import com.ftn.web.service.UserService;
import lombok.val;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
public class KomentarController {
    private final KomentarService komentarService;
    private final UserService userService;
    private final KnjigaService knjigaService;
    private final KomentarRepository komentarRepository;

    public KomentarController(KomentarService komentarService, UserService userService, KnjigaService knjigaService, KomentarRepository komentarRepository) {
        this.komentarService = komentarService;
        this.userService = userService;
        this.knjigaService = knjigaService;
        this.komentarRepository = komentarRepository;
    }

    @PostMapping("/dodajKomentar/{ISBN}")
    public String dodajKomentar(@ModelAttribute("komentarObj") Komentar komentar,
                                @PathVariable("ISBN") String ISBN,
                                Authentication auth) {
        User user = userService.findByUserName(auth.getName());
        komentar.setKorisnik(user);
        komentar.setKnjiga(knjigaService.findByISBN(ISBN));
        komentarService.save(komentar);
        return "page_success";
    }


    @PostMapping("/obrisiKomentar/{ID}")
    public String obrisiKomentar(@PathVariable("ID") int komentarID,Model model) {
        komentarService.obradiKomentar(komentarID, "odbijen");
        val komentari = komentarRepository.findAll();
        model.addAttribute("listaKomentara", komentari);
        return "page_success";
    }

    @PostMapping("/odobriKomentar/{ID}")
    public String odobriKomentar(@PathVariable("ID") int komentarID,Model model) {
        komentarService.obradiKomentar(komentarID, "prihvacen");
        val komentari = komentarRepository.findAll();
        model.addAttribute("listaKomentara", komentari);
        return "page_success";
    }

    @GetMapping("/obradiKomentar")
    public String obradiKomentar(Model model) {
        val komentari = komentarRepository.findAll();
        model.addAttribute("listaKomentara", komentari);
        return "adminKomentari";
    }
}
