package com.ftn.web.controller;

import com.ftn.web.model.*;
import com.ftn.web.service.KnjigaService;
import com.ftn.web.service.ZanrService;
import com.ftn.web.service.impl.KupljenaKnjigaServiceImpl;
import com.ftn.web.service.UserService;
import com.ftn.web.util.DTOConverter;
import lombok.val;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@Controller
public class KnjigaController {

    private final KupljenaKnjigaServiceImpl kupljenaKnjigaService;
    private final KnjigaService knjigaService;
    private final UserService userService;
    private final ZanrService zanrService;
    private final DTOConverter dtoConverter;

    public KnjigaController(KupljenaKnjigaServiceImpl kupljenaKnjigaService, KnjigaService knjigaService, UserService userService, ZanrService zanrService, DTOConverter dtoConverter) {
        this.kupljenaKnjigaService = kupljenaKnjigaService;
        this.knjigaService = knjigaService;
        this.userService = userService;
        this.zanrService = zanrService;
        this.dtoConverter = dtoConverter;
    }



    @PostMapping("/izmeniKnjigu")
    public String izmeniKnjigu(@Valid @ModelAttribute("knjiga") KnjigaDTOIzmena knjigaDTO,BindingResult result, Model model) {
        if(result.hasErrors()) {
            val listZanr = zanrService.findAll();
            List<Integer> listZanrKnjigaID = new ArrayList<>();
            if(knjigaDTO.getZanrList() != null) {
                 listZanrKnjigaID = knjigaDTO.getZanrList();
            }
            model.addAttribute("listZanr", listZanr);
            model.addAttribute("listSvihZanrovaID",listZanrKnjigaID);
            return "knjigaDetails";
        }
        else {
            Knjiga knjiga = dtoConverter.getFromDto(knjigaDTO);
            knjigaService.update(knjiga);
            return "page_success";
        }
    }

    @PostMapping("/poruciKnjigu")
    public String poruciKnjigu(@RequestParam(value = "ISBN") String ISBN,
                               @RequestParam(value = "kolicina") Integer kolicina) {
        Knjiga knjiga = knjigaService.findByISBN(ISBN);
        knjigaService.poruciKnjigu(kolicina,knjiga);
        return "redirect:/";
    }

    @PostMapping("/dodajKnjigu")
    public String dodajKnjigu(@Valid @ModelAttribute("knjiga") KnjigaDTO knjigaDTO,
                                              BindingResult bindingResult,Model model){
        val sviISBN = knjigaService.findAllISBN();
        if(bindingResult.hasErrors()){
            val listZanr = zanrService.findAll();
            model.addAttribute("zanrList", listZanr);
            return "new_knjiga";
        }
        else{
            Knjiga knjiga = dtoConverter.getFromDTO(knjigaDTO);
            knjigaService.save(knjiga);
            return "page_success";
        }
    }

    @GetMapping("/dodajKnjigu")
    public String dodajKnjigu(Model model) {
        Knjiga knjiga = new Knjiga();
        val listZanr = zanrService.findAll();
        model.addAttribute("knjiga", knjiga);
        model.addAttribute("zanrList", listZanr);
        return "new_knjiga";
    }
}
