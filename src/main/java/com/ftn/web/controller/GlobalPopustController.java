package com.ftn.web.controller;

import com.ftn.web.model.GlobalPopust;
import com.ftn.web.repository.GlobalPopustRepository;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Controller
@RequestMapping("/dodajPopust")
public class GlobalPopustController {

    private final GlobalPopustRepository globalPopustRepository;

    public GlobalPopustController(GlobalPopustRepository globalPopustRepository) {
        this.globalPopustRepository = globalPopustRepository;
    }

    @PostMapping
    public String dodajPopust(@Valid @ModelAttribute("globalPopust") GlobalPopust globalPopust, BindingResult bindingResult){
        if(bindingResult.hasErrors()) {
            return "special_popust";
        }
        else{
            globalPopustRepository.save(globalPopust);
            return "page_success";
        }
    }
    @GetMapping
    public String dodajPopust(Model model) {
        GlobalPopust globalPopust = new GlobalPopust();
        model.addAttribute("globalPopust", globalPopust);
        return "special_popust";
    }



}
