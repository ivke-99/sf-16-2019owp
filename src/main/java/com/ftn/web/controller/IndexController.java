package com.ftn.web.controller;

import com.ftn.web.model.Knjiga;
import com.ftn.web.model.Komentar;
import com.ftn.web.model.User;
import com.ftn.web.model.Zanr;
import com.ftn.web.repository.*;
import com.ftn.web.service.*;
import lombok.val;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

import java.security.Principal;
import java.util.ArrayList;
import java.util.stream.Collectors;

@Controller
public class IndexController {


    private final KnjigaRepository knjigaRepository;
    private final UserService userService;
    private final KnjigaService knjigaService;
    private final ZanrService zanrService;
    private final KomentarService komentarService;
    private final KupovinaService kupovinaService;

    public IndexController(KnjigaRepository knjigaRepository, UserService userService, KnjigaService knjigaService, ZanrService zanrService, KomentarService komentarService, KupovinaService kupovinaService) {
        this.knjigaRepository = knjigaRepository;
        this.userService = userService;
        this.knjigaService = knjigaService;
        this.zanrService = zanrService;
        this.komentarService = komentarService;
        this.kupovinaService = kupovinaService;
    }

    @GetMapping("/filterByZanr")
    public String filterByZanr(int zanrID, Model model) {
        val zanr = zanrService.findByID(zanrID);
        val zanrovi = zanrService.findAll();
        val listBooks = new ArrayList<>();
        for(Knjiga k : knjigaRepository.findAll()) {
            for(Zanr z : k.getZanrList()) {
                if(z.getId() == zanrID) {
                    listBooks.add(k);
                }
            }
        }
        model.addAttribute("listBooks", listBooks);
        model.addAttribute("listZanr", zanrovi);
        return "index";
    }

    @GetMapping("/search")
    public String searchResult(@RequestParam(value = "naziv", required = false) String naziv,
                               @RequestParam(value = "autori", required = false) String autori,
                               @RequestParam(value = "jezik_knjige", required = false) String jezik_knjige,
                               @RequestParam(value = "min_cena", required = false) Double min_cena,
                               @RequestParam(value = "max_cena", required = false) Double max_cena,
                               @RequestParam(value = "ISBN", required = false) String ISBN,
                               Model model) {
        if (min_cena == null) {
            min_cena = 0.0;
        }
        if (max_cena == null) {
            max_cena = 50000.0;
        }
        val listBooks = knjigaRepository.searchByParams(naziv, autori, jezik_knjige, min_cena, max_cena, ISBN);
        val listZanr = zanrService.findAll();
        model.addAttribute("listZanr", listZanr);
        model.addAttribute("listBooks", listBooks);
        return "index";
    }

    @GetMapping("/")
    public String indexForm(Model model, Principal principal) {
        if (principal == null) {
            val listBooks = knjigaService.findAll();
            val listZanr = zanrService.findAll();
            model.addAttribute("listBooks", listBooks);
            model.addAttribute("listZanr", listZanr);
            return "index";
        } else {
            val user = userService.findByUserName(principal.getName());
            val listBooks = knjigaService.findAll();
            val listZanr = zanrService.findAll();
            model.addAttribute("user", user);
            model.addAttribute("listBooks", listBooks);
            model.addAttribute("listZanr", listZanr);
            return "index";
        }
    }

    @GetMapping("/showDetails/{ISBN}")
    public String showDetails(@PathVariable(value = "ISBN") String ISBN, Model model,Authentication auth) {
        User user = userService.findByUserName(auth.getName());
        Knjiga knjiga = knjigaService.findByISBN(ISBN);
        val listZanr = zanrService.findAll();
        val listZanrKnjigaID = knjiga.getZanrList().stream().map(Zanr::getId).collect(Collectors.toList());
        model.addAttribute("knjiga", knjiga);
        model.addAttribute("listZanr", listZanr);
        model.addAttribute("listSvihZanrovaID", listZanrKnjigaID);
        if(user.getRoles().equals("ROLE_USER")) {
            val komentari = komentarService.findByBook(knjiga);
            model.addAttribute("komentari", komentari);
        }
        if(kupovinaService.checkIfBought(knjiga,user)) {
            Komentar k = new Komentar();
            model.addAttribute("isBought", true);
            model.addAttribute("ISBN",knjiga.getISBN());
            model.addAttribute("komentarObj", k);
        }
        return "knjigaDetails";
    }

    @GetMapping("/page_success")
    public String successPage() {
        return "page_success";
    }

}
