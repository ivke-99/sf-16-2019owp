package com.ftn.web.controller;

import com.ftn.web.model.Knjiga;
import com.ftn.web.model.KupljenaKnjiga;
import com.ftn.web.repository.KupovinaRepository;
import com.ftn.web.service.KupovinaService;
import com.ftn.web.service.UserService;
import lombok.val;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.stream.Collectors;

@Controller
public class IstorijaKupovinaController {

    private final UserService userService;
    private final KupovinaService kupovinaService;
    private final KupovinaRepository kupovinaRepository;

    public IstorijaKupovinaController(UserService userService, KupovinaService kupovinaService, KupovinaRepository kupovinaRepository) {
        this.userService = userService;
        this.kupovinaService = kupovinaService;
        this.kupovinaRepository = kupovinaRepository;
    }

    @GetMapping("/detaljiKupovine/{id}")
    public String prikaziDetaljeKupovine(Model model,@PathVariable("id") int idKupovine) {
        val kupovina = kupovinaService.findById(idKupovine);
        val ukupanBrojKnjiga =  kupovina.getKupljenaKnjigaList().stream().mapToInt(KupljenaKnjiga::getBrojPrimeraka).sum();
        val naziviKupljenihKnjiga = kupovina.getKupljenaKnjigaList().stream().map(KupljenaKnjiga::getKnjiga).map(Knjiga::getNaziv).collect(Collectors.joining(","));
        model.addAttribute("kupovina", kupovina);
        model.addAttribute("brojKnjiga", ukupanBrojKnjiga);
        model.addAttribute("kupljeneKnjige", naziviKupljenihKnjiga);
        return "detalji_kupovine";
    }

    @GetMapping("/prikaziKupovine")
    public String prikaziKorisnikoveKupovine(Authentication auth, Model model) {
        val user = userService.findByUserName(auth.getName()); // still needs testing
        val listaKupovina = kupovinaRepository.getUserPurchases(user);
        model.addAttribute("listaKupovina", listaKupovina);
        return "istorija_kupovina";
    }

    @GetMapping("/adminPrikaziKupovine/{userName}")
    public String prikaziKorisnikoveKupovine(Model model, @PathVariable("userName") String userName) {
        val user = userService.findByUserName(userName);
        val listaKupovina = kupovinaRepository.getUserPurchases(user);
        model.addAttribute("listaKupovina", listaKupovina);
        return "istorija_kupovina";
    }

}
